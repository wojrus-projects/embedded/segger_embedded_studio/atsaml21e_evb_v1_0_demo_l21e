#ifndef KEYPAD_INCLUDED_H
#define KEYPAD_INCLUDED_H

#include <stdint.h>
#include <stdbool.h>

#include "Board.h"

#ifdef __cplusplus
extern "C" {
#endif


void Keypad_Initialize(void);
void Keypad_DebouncerTask(void);
bool Keypad_GetButtonState(enum Button buttonIndex, bool * pOutButtonState);


#ifdef __cplusplus
}
#endif

#endif // KEYPAD_INCLUDED_H
