#ifndef BOARD_INCLUDED_H
#define BOARD_INCLUDED_H

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif


#define SYSTEM_CLOCK_FREQUENCY_Hz      48000000ul


//
// GPIO pins assignment
//

// Board pins
#define PIN_LED_1           9       // GPIO-OUT
#define PIN_LED_2           10      // GPIO-OUT
#define PIN_BUTTON_1        8       // GPIO-IN/NMI
#define PIN_BUTTON_2        11      // GPIO-IN/EXTINT[11]
#define PIN_CLICK_AN        2       // AIN[0]/DAC-VOUT[0]
#define PIN_CLICK_RST       3       // GPIO-OUT
#define PIN_CLICK_CS        6       // SERCOM0-PAD[2]
#define PIN_CLICK_SCK       5       // SERCOM0-PAD[1]
#define PIN_CLICK_MISO      7       // SERCOM0-PAD[3]
#define PIN_CLICK_MOSI      4       // SERCOM0-PAD[0]
#define PIN_CLICK_PWM       18      // TC4-WO[0]/TCC0-WO[2]
#define PIN_CLICK_INT       27      // EXTINT[15]/GCLK_IO[0]
#define PIN_CLICK_RX        17      // SERCOM1-PAD[1]
#define PIN_CLICK_TX        16      // SERCOM1-PAD[0]
#define PIN_CLICK_SCL       23      // SERCOM3-PAD[1]
#define PIN_CLICK_SDA       22      // SERCOM3-PAD[0]
#define PIN_USB_DM          24      // USB-DM
#define PIN_USB_DP          25      // USB-DP
#define PIN_VUSB_DET        19      // GPIO-IN/EXTINT[3]

#define PORT_LED_1          0
#define PORT_LED_2          0
#define PORT_BUTTON_1       0
#define PORT_BUTTON_2       0
#define PORT_CLICK_AN       0
#define PORT_CLICK_RST      0
#define PORT_CLICK_CS       0
#define PORT_CLICK_SCK      0
#define PORT_CLICK_MISO     0
#define PORT_CLICK_MOSI     0
#define PORT_CLICK_PWM      0
#define PORT_CLICK_INT      0
#define PORT_CLICK_RX       0
#define PORT_CLICK_TX       0
#define PORT_CLICK_SCL      0
#define PORT_CLICK_SDA      0
#define PORT_USB_DM         0
#define PORT_USB_DP         0
#define PORT_VUSB_DET       0

// Diagnostic pins
#define PIN_DAC_0           PIN_CLICK_AN
#define PIN_DAC_1           PIN_CLICK_SCK
#define PIN_CLK_USB         PIN_CLICK_SCL
#define PIN_CLK_SYS         PIN_CLICK_SDA
#define PIN_UART_RTS        PIN_CLICK_PWM       // SERCOM1-PAD[2]
#define PIN_UART_CTS        PIN_VUSB_DET        // SERCOM1-PAD[3]

#define PORT_DAC_0          PORT_CLICK_AN
#define PORT_DAC_1          PORT_CLICK_SCK
#define PORT_CLK_USB        PORT_CLICK_SCL
#define PORT_CLK_SYS        PORT_CLICK_SDA
#define PORT_UART_RTS       PORT_CLICK_PWM
#define PORT_UART_CTS       PORT_VUSB_DET

//
// LED / button instance numbers
//

#define LED_NUM         2
#define BUTTON_NUM      2

//
// LED indexes
//

enum LED
{
    // LED by index
    LED_1 = 0,
    LED_2 = 1,

    // LED by color
    LED_GREEN = LED_1,
    LED_RED = LED_2
};

//
// Button indexes
//

enum Button
{
    Button_1 = 0,
    Button_2 = 1
};


void Board_Initialize(void);
void LED_Set(enum LED ledIndex, bool state);
void LED_Toggle(enum LED ledIndex);
bool Button_Get(enum Button buttonIndex);


#ifdef __cplusplus
}
#endif

#endif // BOARD_INCLUDED_H
