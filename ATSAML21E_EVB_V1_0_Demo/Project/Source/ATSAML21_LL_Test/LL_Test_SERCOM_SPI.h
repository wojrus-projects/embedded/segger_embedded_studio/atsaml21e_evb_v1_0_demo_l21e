#ifndef LL_TEST_SERCOM_SPI_INCLUDED_H
#define LL_TEST_SERCOM_SPI_INCLUDED_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


void LL_Test_SERCOM_SPI_Master_Simple(uint32_t dataSizeBits);
void LL_Test_SERCOM_SPI_Master_DMA(void);

void LL_Test_SERCOM_SPI_Master_InterruptHandlerSPI(void);
void LL_Test_SERCOM_SPI_Master_InterruptHandlerDMAC(void);


#ifdef __cplusplus
}
#endif

#endif // LL_TEST_SERCOM_SPI_INCLUDED_H
