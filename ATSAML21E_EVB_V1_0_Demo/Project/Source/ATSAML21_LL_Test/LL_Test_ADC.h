#ifndef LL_TEST_ADC_INCLUDED_H
#define LL_TEST_ADC_INCLUDED_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif


enum LL_Test_ADC_Resolution
{
    LL_Test_ADC_Resolution_12bit,
    LL_Test_ADC_Resolution_16bit
};


void LL_Test_ADC_Simple(enum LL_Test_ADC_Resolution adcResolution);
void LL_Test_ADC_DMA(enum LL_Test_ADC_Resolution adcResolution, bool oneShot);


#ifdef __cplusplus
}
#endif

#endif // LL_TEST_ADC_INCLUDED_H
