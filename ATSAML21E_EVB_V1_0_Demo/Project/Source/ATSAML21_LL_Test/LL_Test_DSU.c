//
// Testy LL_DSU.
//

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <assert.h>
#include <stdalign.h>

#include "ATSAML21_LL/LL.h"
#include "Board.h"


//
// Function: LL_Test_DSU_Calculate_CRC32
//
// Testy CRC32 dla różnych danych (ROM/RAM).
// Wyniki są zweryfikowane przez:
// https://www.scadacore.com/tools/programming-calculators/online-checksum-calculator/
//
// Parametry CRC32:
// - generator: 0xEDB88320 (reversed representation)
// - initial value: 0xFFFFFFFF
// - final XOR: 0xFFFFFFFF
// - little endian
//
void LL_Test_DSU_Calculate_CRC32(void)
{
    printf("LL_Test_DSU_Calculate_CRC32\n");

    const uint32_t crcInitialValue = 0xFFFFFFFF;

    const uint32_t * pInputDataAddress;
    uint32_t crcOutput;
    bool crcStatus;

    //
    // Test data nr 1 (ROM).
    //

    alignas (4) static const uint32_t testVector1[4] =
    {
        0x00000000,
        0x11000000,
        0x00000000,
        0x22000000
    };

    const uint32_t testVector1CRC = 0x82605C83;

    //
    // Test data nr 2 (ROM).
    //

    alignas (4) static const uint8_t testVector2[8] =
    {
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
        '8'
    };

    const uint32_t testVector2CRC = 0x9AE0DAAF;

    //
    // Test data nr 3 (ROM).
    //

    alignas (4) static const uint32_t testVector3[10] =
    {
        0x00000000, 0x11111111, 0x22222222, 0x33333333, 0x44444444,
        0x55555555, 0x66666666, 0x77777777, 0x88888888, 0x99999999
    };

    const uint32_t testVector3CRC = 0xF8EE400B;

    //
    // Test data nr 4 (RAM).
    //

    alignas (4) const uint32_t testVector4[10] =
    {
        0x00000000, 0x11111111, 0x22222222, 0x33333333, 0x44444444,
        0x55555555, 0x66666666, 0x77777777, 0x88888888, 0x99999999
    };

    const uint32_t testVector4CRC = testVector3CRC;

    //
    // Wyłączenie w PAC blokady zapisu rejestrów DSU.
    //

    // Datasheet: Figure 10-1. Atmel SAM L21 Product Mapping.
    const uint32_t DSU_Bridge = 1; // AHB-APB Bridge B
    const uint32_t DSU_Index = 1;

    LL_PAC_SetWriteControl(LL_PAC_ControlMode_CLEAR, DSU_Bridge, DSU_Index);

    //
    // Test nr 1.
    //

    LL_DSU_Reset();
    pInputDataAddress = &testVector1[0];
    crcStatus = LL_DSU_CalculateCRC32(pInputDataAddress, 4, crcInitialValue, &crcOutput);

    printf("address: 0x%08X, crcStatus: %u, crcOutput: 0x%08X, XOR(crcOutput): 0x%08X, valid: %u\n",
        pInputDataAddress, crcStatus, crcOutput, crcOutput ^ 0xFFFFFFFFul, (crcOutput ^ 0xFFFFFFFFul) == testVector1CRC);

    //
    // Test nr 2.
    //

    LL_DSU_Reset();
    pInputDataAddress = (const uint32_t *)&testVector2[0];
    crcStatus = LL_DSU_CalculateCRC32(pInputDataAddress, 2, crcInitialValue, &crcOutput);

    printf("address: 0x%08X, crcStatus: %u, crcOutput: 0x%08X, XOR(crcOutput): 0x%08X, valid: %u\n",
        pInputDataAddress, crcStatus, crcOutput, crcOutput ^ 0xFFFFFFFFul, (crcOutput ^ 0xFFFFFFFFul) == testVector2CRC);

    //
    // Test nr 3.
    //

    LL_DSU_Reset();
    pInputDataAddress = &testVector3[0];
    crcStatus = LL_DSU_CalculateCRC32(pInputDataAddress, 10, crcInitialValue, &crcOutput);

    printf("address: 0x%08X, crcStatus: %u, crcOutput: 0x%08X, XOR(crcOutput): 0x%08X, valid: %u\n",
        pInputDataAddress, crcStatus, crcOutput, crcOutput ^ 0xFFFFFFFFul, (crcOutput ^ 0xFFFFFFFFul) == testVector3CRC);

    //
    // Test nr 4.
    //

    LL_DSU_Reset();
    pInputDataAddress = &testVector4[0];
    crcStatus = LL_DSU_CalculateCRC32(pInputDataAddress, 10, crcInitialValue, &crcOutput);

    printf("address: 0x%08X, crcStatus: %u, crcOutput: 0x%08X, XOR(crcOutput): 0x%08X, valid: %u\n",
        pInputDataAddress, crcStatus, crcOutput, crcOutput ^ 0xFFFFFFFFul, (crcOutput ^ 0xFFFFFFFFul) == testVector4CRC);

    for (;;)
    {
    }
}
