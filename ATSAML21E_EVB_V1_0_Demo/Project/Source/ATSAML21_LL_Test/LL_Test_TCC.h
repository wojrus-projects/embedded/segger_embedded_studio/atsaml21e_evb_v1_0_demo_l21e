#ifndef LL_TEST_TCC_INCLUDED_H
#define LL_TEST_TCC_INCLUDED_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif


void LL_Test_TCC0_Count_Buttons(void);
void LL_Test_TCC0_Counter_External(void);
void LL_Test_TCC0_PWM_Simple(void);


#ifdef __cplusplus
}
#endif

#endif // LL_TEST_TCC_INCLUDED_H
