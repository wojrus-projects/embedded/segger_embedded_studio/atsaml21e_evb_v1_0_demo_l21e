//
// Peripheral Driver Low Layer (DAC module).
//

#ifndef LL_DAC_INCLUDED
#define LL_DAC_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


// Number of DACs
#define LL_DAC_NUM      DAC_DAC_NUM


enum LL_DAC_Status
{
    LL_DAC_Status_READY0 = 1 << 0,
    LL_DAC_Status_READY1 = 1 << 1,
    LL_DAC_Status_EOC0   = 1 << 2,
    LL_DAC_Status_EOC1   = 1 << 3
};


enum LL_DAC_ReferenceVoltage
{
    LL_DAC_ReferenceVoltage_VREFAU = 0x0,
    LL_DAC_ReferenceVoltage_VDDANA = 0x1,
    LL_DAC_ReferenceVoltage_VREFAB = 0x2,
    LL_DAC_ReferenceVoltage_INTREF = 0x3
};


enum LL_DAC_InterruptFlag
{
    LL_DAC_InterruptFlag_UNDERRUN0 = 1 << 0,
    LL_DAC_InterruptFlag_UNDERRUN1 = 1 << 1,
    LL_DAC_InterruptFlag_EMPTY0    = 1 << 2,
    LL_DAC_InterruptFlag_EMPTY1    = 1 << 3
};


enum LL_DAC_Current
{
    LL_DAC_Current_CC100K = 0x0,
    LL_DAC_Current_CC1M   = 0x1,
    LL_DAC_Current_CC12M  = 0x2

};


enum LL_DAC_RefreshPeriod
{
    LL_DAC_RefreshPeriod_DISABLED = 0x0,
    LL_DAC_RefreshPeriod_30us     = 0x1,
    LL_DAC_RefreshPeriod_60us     = 0x2,
    LL_DAC_RefreshPeriod_90us     = 0x3,
    LL_DAC_RefreshPeriod_120us    = 0x4,
    LL_DAC_RefreshPeriod_150us    = 0x5,
    LL_DAC_RefreshPeriod_180us    = 0x6,
    LL_DAC_RefreshPeriod_210us    = 0x7,
    LL_DAC_RefreshPeriod_240us    = 0x8,
    LL_DAC_RefreshPeriod_270us    = 0x9,
    LL_DAC_RefreshPeriod_300us    = 0xA,
    LL_DAC_RefreshPeriod_330us    = 0xB,
    LL_DAC_RefreshPeriod_360us    = 0xC,
    LL_DAC_RefreshPeriod_390us    = 0xD,
    LL_DAC_RefreshPeriod_420us    = 0xE,
    LL_DAC_RefreshPeriod_450us    = 0xF
};


void LL_DAC_Reset(void);
void LL_DAC_Enable(uint32_t enable);
void LL_DAC_SetDebugControl(uint32_t runInDebugHaltedState);

enum LL_DAC_Status LL_DAC_GetStatus(void);
void LL_DAC_SetReferenceVoltage(enum LL_DAC_ReferenceVoltage referenceVoltage);
void LL_DAC_EnableEventOutput(uint32_t dacIndex, uint32_t dataBufferEmptyEventOutputEnable);
void LL_DAC_EnableEventInput(uint32_t dacIndex, uint32_t startConversionEventInputEnable, uint32_t invert);

void LL_DAC_InterruptEnableClear(enum LL_DAC_InterruptFlag interruptFlag);
void LL_DAC_InterruptEnableSet(enum LL_DAC_InterruptFlag interruptFlag);
enum LL_DAC_InterruptFlag LL_DAC_GetInterruptFlag(void);
void LL_DAC_ClearInterruptFlag(enum LL_DAC_InterruptFlag interruptFlag);

void LL_DAC_ChannelEnable(uint32_t dacIndex, uint32_t enable, uint32_t runInStandby);
void LL_DAC_ChannelSetConfiguration(uint32_t dacIndex, uint32_t leftAdjustedData, enum LL_DAC_Current currentControl, uint32_t ditheringMode, enum LL_DAC_RefreshPeriod refreshPeriod);
void LL_DAC_ChannelWriteData(uint32_t dacIndex, uint16_t data);
void LL_DAC_ChannelWriteDataBuffer(uint32_t dacIndex, uint16_t data);


#ifdef __cplusplus
}
#endif

#endif // LL_DAC_INCLUDED
