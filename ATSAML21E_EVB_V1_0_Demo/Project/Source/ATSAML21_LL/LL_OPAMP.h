//
// Peripheral Driver Low Layer (OPAMP module).
//

#ifndef LL_OPAMP_INCLUDED
#define LL_OPAMP_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


// Number of opamps
#define LL_OPAMP_NUM        2


enum LL_OPAMP_Status
{
    LL_OPAMP_Status_READY0 = 1 << 0,
    LL_OPAMP_Status_READY1 = 1 << 1,
    LL_OPAMP_Status_READY2 = 1 << 2
};


enum LL_OPAMP_PositiveInputMux
{
    LL_OPAMP_PositiveInputMux_012_OAxPOS = 0x0,
    LL_OPAMP_PositiveInputMux_012_OAxTAP = 0x1,
    LL_OPAMP_PositiveInputMux_0_DAC      = 0x2,
    LL_OPAMP_PositiveInputMux_1_OA0OUT   = 0x2,
    LL_OPAMP_PositiveInputMux_2_OA1OUT   = 0x2,
    LL_OPAMP_PositiveInputMux_012_GND    = 0x3,
    LL_OPAMP_PositiveInputMux_2_OA0POS   = 0x4,
    LL_OPAMP_PositiveInputMux_2_OA1POS   = 0x5,
    LL_OPAMP_PositiveInputMux_2_OA0TAP   = 0x6
};


enum LL_OPAMP_NegativeInputMux
{
    LL_OPAMP_NegativeInputMux_012_OAxNEG = 0x0,
    LL_OPAMP_NegativeInputMux_012_OAxTAP = 0x1,
    LL_OPAMP_NegativeInputMux_012_OAxOUT = 0x2,
    LL_OPAMP_NegativeInputMux_01_DAC     = 0x3,
    LL_OPAMP_NegativeInputMux_2_OA0NEG   = 0x3,
    LL_OPAMP_NegativeInputMux_2_OA1NEG   = 0x4,
    LL_OPAMP_NegativeInputMux_2_DAC      = 0x5
};


enum LL_OPAMP_Bias
{
    LL_OPAMP_Bias_Mode0_Slowest = 0x0,
    LL_OPAMP_Bias_Mode1         = 0x1,
    LL_OPAMP_Bias_Mode2         = 0x2,
    LL_OPAMP_Bias_Mode3_Fastest = 0x3
};


enum LL_OPAMP_Resistor1Mux
{
    LL_OPAMP_Resistor1Mux_012_OAxPOS = 0x0,
    LL_OPAMP_Resistor1Mux_012_OAxNEG = 0x1,
    LL_OPAMP_Resistor1Mux_0_DAC      = 0x2,
    LL_OPAMP_Resistor1Mux_1_OA0OUT   = 0x2,
    LL_OPAMP_Resistor1Mux_2_OA1OUT   = 0x2,
    LL_OPAMP_Resistor1Mux_012_GND    = 0x3
};


enum LL_OPAMP_Potentiometer
{
    LL_OPAMP_Potentiometer_Gain_1_DIV_7  = 0x0,
    LL_OPAMP_Potentiometer_Gain_1_DIV_3  = 0x1,
    LL_OPAMP_Potentiometer_Gain_1        = 0x2,
    LL_OPAMP_Potentiometer_Gain_5_DIV_3  = 0x3,
    LL_OPAMP_Potentiometer_Gain_3        = 0x4,
    LL_OPAMP_Potentiometer_Gain_13_DIV_3 = 0x5,
    LL_OPAMP_Potentiometer_Gain_7        = 0x6,
    LL_OPAMP_Potentiometer_Gain_15       = 0x7
};


void LL_OPAMP_Reset(void);
void LL_OPAMP_Enable(uint32_t enable, uint32_t lowPowerInputMuxEnable);
enum LL_OPAMP_Status LL_OPAMP_GetStatus(void);
void LL_OPAMP_SetInputMux(uint32_t opampIndex, enum LL_OPAMP_PositiveInputMux positiveInputMux, enum LL_OPAMP_NegativeInputMux negativeInputMux);
void LL_OPAMP_SetResistors(uint32_t opampIndex, uint32_t resistorLadderToOutputEnable, uint32_t resistorLadderToVCCEnable, uint32_t resistor1Enable, enum LL_OPAMP_Resistor1Mux resistor1Mux, enum LL_OPAMP_Potentiometer potentiometer);
void LL_OPAMP_EnableOpamp(uint32_t opampIndex, uint32_t enable, uint32_t analogOutputEnable, enum LL_OPAMP_Bias bias, uint32_t runInStandby, uint32_t onDemand);


#ifdef __cplusplus
}
#endif

#endif // LL_OPAMP_INCLUDED
