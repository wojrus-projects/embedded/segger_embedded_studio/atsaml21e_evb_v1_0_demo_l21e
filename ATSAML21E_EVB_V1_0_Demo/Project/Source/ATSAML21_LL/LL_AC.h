//
// Peripheral Driver Low Layer (AC module).
//

#ifndef LL_AC_INCLUDED
#define LL_AC_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


// Number of comparators
#define LL_AC_NUM       AC_NUM_CMP


enum LL_AC_InterruptFlag
{
    LL_AC_InterruptFlag_COMP0 = 1 << 0,
    LL_AC_InterruptFlag_COMP1 = 1 << 1,
    LL_AC_InterruptFlag_WIN0  = 1 << 4
};


enum LL_AC_InterruptMode
{
    LL_AC_InterruptMode_TOGGLE  = 0x0,
    LL_AC_InterruptMode_RISING  = 0x1,
    LL_AC_InterruptMode_FALLING = 0x2,
    LL_AC_InterruptMode_EOC     = 0x3
};


enum LL_AC_WindowState
{
    LL_AC_WindowState_ABOVE  = 0x0,
    LL_AC_WindowState_INSIDE = 0x1,
    LL_AC_WindowState_BELOW  = 0x2
};


enum AC_LL_WindowMode
{
    AC_LL_WindowMode_ABOVE   = 0x0,
    AC_LL_WindowMode_INSIDE  = 0x1,
    AC_LL_WindowMode_BELOW   = 0x2,
    AC_LL_WindowMode_OUTSIDE = 0x3
};


enum AC_LL_NegativeInputMux
{
    AC_LL_NegativeInputMux_PIN0      = 0x0,
    AC_LL_NegativeInputMux_PIN1      = 0x1,
    AC_LL_NegativeInputMux_PIN2      = 0x2,
    AC_LL_NegativeInputMux_PIN3      = 0x3,
    AC_LL_NegativeInputMux_GND       = 0x4,
    AC_LL_NegativeInputMux_VSCALE    = 0x5,
    AC_LL_NegativeInputMux_BANDGAP   = 0x6,
    AC_LL_NegativeInputMux_DAC_OPAMP = 0x7
};


enum AC_LL_PositiveInputMux
{
    AC_LL_PositiveInputMux_PIN0   = 0x0,
    AC_LL_PositiveInputMux_PIN1   = 0x1,
    AC_LL_PositiveInputMux_PIN2   = 0x2,
    AC_LL_PositiveInputMux_PIN3   = 0x3,
    AC_LL_PositiveInputMux_VSCALE = 0x4
};


enum AC_LL_Speed
{
    AC_LL_Speed_LOW     = 0x0,
    AC_LL_Speed_MEDLOW  = 0x1,
    AC_LL_Speed_MEDHIGH = 0x2,
    AC_LL_Speed_HIGH    = 0x3
};


enum AC_LL_Hysteresis
{
    AC_LL_Hysteresis_HYST50  = 0x0,
    AC_LL_Hysteresis_HYST70  = 0x1,
    AC_LL_Hysteresis_HYST90  = 0x2,
    AC_LL_Hysteresis_HYST110 = 0x3
};


enum AC_LL_Filter
{
    AC_LL_Filter_OFF  = 0x0,
    AC_LL_Filter_MAJ3 = 0x1,
    AC_LL_Filter_MAJ5 = 0x2
};


enum AC_LL_Output
{
    AC_LL_Output_OFF   = 0x0,
    AC_LL_Output_ASYNC = 0x1,
    AC_LL_Output_SYNC  = 0x2
};


void LL_AC_Reset(void);
void LL_AC_Enable(uint32_t enable);
void LL_AC_SetDebugControl(uint32_t runInDebugHaltedState);
void LL_AC_StartSingleComparison(uint32_t startComparator0, uint32_t startComparator1);

void LL_AC_EnableWindowMode(uint32_t enable, enum AC_LL_WindowMode windowMode);
void LL_AC_EnableEventInput(uint32_t acIndex, uint32_t enable, uint32_t invertInput);
void LL_AC_EnableEventOutput(uint32_t comparator0OutputEventEnable, uint32_t comparator1OutputEventEnable, uint32_t windowEventEnable);
void LL_AC_SetScaler(uint32_t acIndex, uint32_t value);

void LL_AC_EnableChannel(uint32_t acIndex, uint32_t enable, uint32_t runInStandby);
void LL_AC_EnableSingleShotMode(uint32_t acIndex, uint32_t enable);
void LL_AC_SetInterruptOrEventMode(uint32_t acIndex, enum LL_AC_InterruptMode mode);
void LL_AC_SetNegativeInputMux(uint32_t acIndex, enum AC_LL_NegativeInputMux negativeInputMux);
void LL_AC_SetPositiveInputMux(uint32_t acIndex, enum AC_LL_PositiveInputMux positiveInputMux);
void LL_AC_SwapInputs(uint32_t acIndex, uint32_t swapInputs);
void LL_AC_SetSpeed(uint32_t acIndex, enum AC_LL_Speed speed);
void LL_AC_EnableHysteresis(uint32_t acIndex, uint32_t enable, enum AC_LL_Hysteresis hysteresisLevel);
void LL_AC_SetFilter(uint32_t acIndex, enum AC_LL_Filter filterLength);
void LL_AC_SetOutput(uint32_t acIndex, enum AC_LL_Output output);

void LL_AC_InterruptEnableClear(enum LL_AC_InterruptFlag interruptFlag);
void LL_AC_InterruptEnableSet(enum LL_AC_InterruptFlag interruptFlag);
enum LL_AC_InterruptFlag LL_AC_GetInterruptFlag(void);
void LL_AC_ClearInterruptFlag(enum LL_AC_InterruptFlag interruptFlag);

uint32_t LL_AC_IsOutputReady(uint32_t acIndex);
uint32_t LL_AC_GetOutputState(uint32_t acIndex);
enum LL_AC_WindowState LL_AC_GetWindowState(void);


#ifdef __cplusplus
}
#endif

#endif // LL_AC_INCLUDED
