#include <sam.h>
#include "LL_PORT.h"


void LL_PORT_SetDirection(uint32_t portNumber, uint32_t pinNumber, enum LL_PORT_PinDirection direction)
{
    if (direction == LL_PORT_PinDirection_Input)
    {
        PORT->Group[portNumber].DIRCLR.reg = 1ul << pinNumber;
    }
    else
    {
        PORT->Group[portNumber].DIRSET.reg = 1ul << pinNumber;
    }
}


void LL_PORT_SetPullMode(uint32_t portNumber, uint32_t pinNumber, enum LL_PORT_PullMode mode)
{
    switch (mode)
    {
        case LL_PORT_PullMode_Disable:
            PORT->Group[portNumber].PINCFG[pinNumber].bit.PULLEN = 0;
            break;

        case LL_PORT_PullMode_Up:
            PORT->Group[portNumber].OUTSET.reg = 1ul << pinNumber;
            PORT->Group[portNumber].PINCFG[pinNumber].bit.PULLEN = 1;
            break;

        case LL_PORT_PullMode_Down:
            PORT->Group[portNumber].OUTCLR.reg = 1ul << pinNumber;
            PORT->Group[portNumber].PINCFG[pinNumber].bit.PULLEN = 1;
            break;

        default:
            return;
    }
}


void LL_PORT_SetOutputDriverStrength(uint32_t portNumber, uint32_t pinNumber, enum LL_PORT_OutputDriverStrength driverStrength)
{
    PORT_PINCFG_Type pinCfg = PORT->Group[portNumber].PINCFG[pinNumber];

    pinCfg.bit.DRVSTR = (driverStrength == LL_PORT_OutputDriverStrength_High);

    PORT->Group[portNumber].PINCFG[pinNumber] = pinCfg;
}


void LL_PORT_EnableInput(uint32_t portNumber, uint32_t pinNumber, uint32_t enable)
{
    PORT_PINCFG_Type pinCfg = PORT->Group[portNumber].PINCFG[pinNumber];

    pinCfg.bit.INEN = (enable != 0);

    PORT->Group[portNumber].PINCFG[pinNumber] = pinCfg;
}


void LL_PORT_EnableInputSynchronizer(uint32_t portNumber, uint32_t pinNumber, uint32_t enable)
{
    if (enable == 0)
    {
        PORT->Group[portNumber].CTRL.reg &= ~(1ul << pinNumber);
    }
    else
    {
        PORT->Group[portNumber].CTRL.reg |= (1ul << pinNumber);
    }
}


void LL_PORT_EnablePeripheralMultiplexer(uint32_t portNumber, uint32_t pinNumber, uint32_t enable)
{
    PORT_PINCFG_Type pinCfg = PORT->Group[portNumber].PINCFG[pinNumber];

    pinCfg.bit.PMUXEN = (enable != 0);

    PORT->Group[portNumber].PINCFG[pinNumber] = pinCfg;
}


void LL_PORT_SetPeripheralMultiplexing(uint32_t portNumber, uint32_t pinNumber, enum LL_PORT_PeripheralFunction peripheralFunction)
{
    PORT_PMUX_Type pmux = PORT->Group[portNumber].PMUX[pinNumber / 2];

    if ((pinNumber % 2) == 0)
    {
        pmux.bit.PMUXE = peripheralFunction & 0xF;
    }
    else
    {
        pmux.bit.PMUXO = peripheralFunction & 0xF;
    }

    PORT->Group[portNumber].PMUX[pinNumber / 2] = pmux;
}


void LL_PORT_SetEventAction(uint32_t portNumber, uint32_t actionIndex, uint32_t enable, enum LL_PORT_EventAction actionType, uint32_t pinNumber)
{
    uint32_t actionConfiguration = ((pinNumber & 0x1F) << PORT_EVCTRL_PID0_Pos)
                                 | ((actionType & 0x3) << PORT_EVCTRL_EVACT0_Pos)
                                 | ((enable != 0)      << PORT_EVCTRL_PORTEI0_Pos);

    actionConfiguration <<= actionIndex * 8;

    PORT_EVCTRL_Type evctrl = PORT->Group[portNumber].EVCTRL;

    evctrl.reg &= ~(0xFFul << actionIndex * 8);
    evctrl.reg |= actionConfiguration;

    PORT->Group[portNumber].EVCTRL = evctrl;
}


void LL_PORT_SetOutput(uint32_t portNumber, uint32_t pinNumber, uint32_t state)
{
    if (state == 0)
    {
        PORT->Group[portNumber].OUTCLR.reg = 1ul << pinNumber;
    }
    else
    {
        PORT->Group[portNumber].OUTSET.reg = 1ul << pinNumber;
    }
}


void LL_PORT_ToggleOutput(uint32_t portNumber, uint32_t pinNumber)
{
    PORT->Group[portNumber].OUTTGL.reg = 1ul << pinNumber;
}


uint32_t LL_PORT_ReadInput(uint32_t portNumber, uint32_t pinNumber)
{
    return (((PORT->Group[portNumber].IN.reg & (1ul << pinNumber))) != 0);
}
