//
// Peripheral Driver Low Layer (EIC module).
//

#ifndef LL_EIC_INCLUDED
#define LL_EIC_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


// Number of EXTINT inputs
#define LL_EIC_EXTINT_NUM        EIC_EXTINT_NUM


enum LL_EIC_Clock
{
    LL_EIC_Clock_GCLK_EIC   = 0,
    LL_EIC_Clock_CLK_ULP32K = 1
};


enum LL_EIC_InputSense
{
    LL_EIC_InputSense_NONE = 0x0,
    LL_EIC_InputSense_RISE = 0x1,
    LL_EIC_InputSense_FALL = 0x2,
    LL_EIC_InputSense_BOTH = 0x3,
    LL_EIC_InputSense_HIGH = 0x4,
    LL_EIC_InputSense_LOW  = 0x5
};


enum LL_EIC_NMI_Sense
{
    LL_EIC_NMI_Sense_NONE = 0x0,
    LL_EIC_NMI_Sense_RISE = 0x1,
    LL_EIC_NMI_Sense_FALL = 0x2,
    LL_EIC_NMI_Sense_BOTH = 0x3,
    LL_EIC_NMI_Sense_HIGH = 0x4,
    LL_EIC_NMI_Sense_LOW  = 0x5
};


enum LL_EIC_InterruptFlag
{
    LL_EIC_InterruptFlag_EXTINT0  = 1 << 0,
    LL_EIC_InterruptFlag_EXTINT1  = 1 << 1,
    LL_EIC_InterruptFlag_EXTINT2  = 1 << 2,
    LL_EIC_InterruptFlag_EXTINT3  = 1 << 3,
    LL_EIC_InterruptFlag_EXTINT4  = 1 << 4,
    LL_EIC_InterruptFlag_EXTINT5  = 1 << 5,
    LL_EIC_InterruptFlag_EXTINT6  = 1 << 6,
    LL_EIC_InterruptFlag_EXTINT7  = 1 << 7,
    LL_EIC_InterruptFlag_EXTINT8  = 1 << 8,
    LL_EIC_InterruptFlag_EXTINT9  = 1 << 9,
    LL_EIC_InterruptFlag_EXTINT10 = 1 << 10,
    LL_EIC_InterruptFlag_EXTINT11 = 1 << 11,
    LL_EIC_InterruptFlag_EXTINT12 = 1 << 12,
    LL_EIC_InterruptFlag_EXTINT13 = 1 << 13,
    LL_EIC_InterruptFlag_EXTINT14 = 1 << 14,
    LL_EIC_InterruptFlag_EXTINT15 = 1 << 15
};


void LL_EIC_Reset(void);
void LL_EIC_Enable(uint32_t enable);
void LL_EIC_SetClock(enum LL_EIC_Clock clock);
void LL_EIC_EnableEventOutput(uint32_t eventOutputBitmap);
void LL_EIC_SetInputConfiguration(uint32_t inputIndex, enum LL_EIC_InputSense senseMode, uint32_t enableFilter);
void LL_EIC_SetAsynchronousEdgeDetectionMode(uint32_t inputBitmap);

void LL_EIC_InitializeNMI(enum LL_EIC_NMI_Sense senseConfiguration, uint32_t filterEnable, uint32_t asynchronousEdgeDetectionMode);
uint32_t LL_EIC_GetNMIInterruptFlag(void);
void LL_EIC_ClearNMIInterruptFlag(void);

void LL_EIC_InterruptEnableClear(enum LL_EIC_InterruptFlag interruptFlag);
void LL_EIC_InterruptEnableSet(enum LL_EIC_InterruptFlag interruptFlag);
enum LL_EIC_InterruptFlag LL_EIC_GetInterruptFlag(void);
void LL_EIC_ClearInterruptFlag(enum LL_EIC_InterruptFlag interruptFlag);


#ifdef __cplusplus
}
#endif

#endif // LL_EIC_INCLUDED
