#include <string.h>

#include <sam.h>
#include "LL_NVMCTRL.h"


struct LL_NVMCTRL_Parameters LL_NVMCTRL_GetParameter(void)
{
    struct LL_NVMCTRL_Parameters parameters;

    NVMCTRL_PARAM_Type param = NVMCTRL->PARAM;

    switch (param.bit.PSZ)
    {
        case 0x0: parameters.PageSizeBytes = 8; break;
        case 0x1: parameters.PageSizeBytes = 16; break;
        case 0x2: parameters.PageSizeBytes = 32; break;
        case 0x3: parameters.PageSizeBytes = 64; break;
        case 0x4: parameters.PageSizeBytes = 128; break;
        case 0x5: parameters.PageSizeBytes = 256; break;
        case 0x6: parameters.PageSizeBytes = 512; break;
        case 0x7: parameters.PageSizeBytes = 1024; break;
    };

    parameters.NVM_PagesNumber = param.bit.NVMP;
    parameters.RWW_EEPROM_PagesNumber = param.bit.RWWEEP;

    return parameters;
}


void LL_NVMCTRL_SetCache(uint32_t cacheDisable, enum LL_NVMCTRL_CacheReadMode cacheReadMode)
{
    NVMCTRL_CTRLB_Type ctrlb = NVMCTRL->CTRLB;

    ctrlb.bit.CACHEDIS = (cacheDisable != 0);
    ctrlb.bit.READMODE = cacheReadMode & 0x3;

    NVMCTRL->CTRLB = ctrlb;
}


void LL_NVMCTRL_InvalidateCache(void)
{
    LL_NVMCTRL_ExecuteCommand(LL_NVMCTRL_Command_INVALL);
}


void LL_NVMCTRL_SetReadWaitStates(uint32_t waitStates)
{
    NVMCTRL_CTRLB_Type ctrlb = NVMCTRL->CTRLB;

    ctrlb.bit.RWS = waitStates & 0xF;

    NVMCTRL->CTRLB = ctrlb;
}


void LL_NVMCTRL_SetPowerReductionMode(enum LL_NVMCTRL_PowerReductionMode mode)
{
    NVMCTRL_CTRLB_Type ctrlb = NVMCTRL->CTRLB;

    ctrlb.bit.SLEEPPRM = mode & 0x3;

    NVMCTRL->CTRLB = ctrlb;
}


void LL_NVMCTRL_ExecuteCommand(enum LL_NVMCTRL_Command command)
{
    NVMCTRL_CTRLA_Type ctrla;

    ctrla.reg = 0;
    ctrla.bit.CMDEX = 0xA5;
    ctrla.bit.CMD = command & 0x7F;

    NVMCTRL->CTRLA = ctrla;
}


void LL_NVMCTRL_SetAddress(uint32_t address)
{
    NVMCTRL->ADDR.reg = address & 0x3FFF;
}


uint32_t LL_NVMCTRL_GetLockSection(void)
{
    return NVMCTRL->LOCK.reg;
}


enum LL_NVMCTRL_Status LL_NVMCTRL_GetStatus(void)
{
    return NVMCTRL->STATUS.reg;
}


void LL_NVMCTRL_ClearStatus(enum LL_NVMCTRL_Status statusBits)
{
    NVMCTRL->STATUS.reg = statusBits;
}


void LL_NVMCTRL_InterruptEnableClear(enum LL_NVMCTRL_InterruptFlag interruptFlag)
{
    NVMCTRL->INTENCLR.reg = interruptFlag;
}


void LL_NVMCTRL_InterruptEnableSet(enum LL_NVMCTRL_InterruptFlag interruptFlag)
{
    NVMCTRL->INTENSET.reg = interruptFlag;
}


enum LL_NVMCTRL_InterruptFlag LL_NVMCTRL_GetInterruptFlag(void)
{
    return NVMCTRL->INTFLAG.reg;
}


void LL_NVMCTRL_ClearInterruptFlag(enum LL_NVMCTRL_InterruptFlag interruptFlag)
{
    NVMCTRL->INTFLAG.reg = interruptFlag;
}


void LL_NVMCTRL_ReadSerialNumber(uint8_t * const pOutSerialNumber)
{
    uint32_t serialNumberWords[4];

    serialNumberWords[0] = *((const uint32_t *)0x0080A00C);
    serialNumberWords[1] = *((const uint32_t *)0x0080A040);
    serialNumberWords[2] = *((const uint32_t *)0x0080A044);
    serialNumberWords[3] = *((const uint32_t *)0x0080A048);

    memcpy(pOutSerialNumber, &serialNumberWords, sizeof(serialNumberWords));
}


struct LL_NVMCTRL_SoftwareCalibrationData LL_NVMCTRL_ReadSoftwareCalibrationData(void)
{
    struct LL_NVMCTRL_SoftwareCalibrationData data;

    memcpy(&data, (const uint8_t *)0x00806020, sizeof(struct LL_NVMCTRL_SoftwareCalibrationData));

    return data;
}


void LL_NVMCTRL_ReadUserRow(struct LL_NVMCTRL_UserRow * const pOutUserRow)
{
    memcpy(pOutUserRow, (const uint8_t *)0x00804000, sizeof(struct LL_NVMCTRL_UserRow));
}


void LL_NVMCTRL_InitializeFactoryUserRow(struct LL_NVMCTRL_UserRow * pUserRow)
{
    pUserRow->BOOTPROT         = 0x7;
    pUserRow->Reserved_1       = 0x1;
    pUserRow->EEPROM           = 0x7;
    pUserRow->Reserved_2       = 0x1;
    pUserRow->BOD33_Level      = 0x06;
    pUserRow->BOD33_Disable    = 0x0;
    pUserRow->BOD33_Action     = 0x1;
    pUserRow->Reserved_3       = 0x08F;
    pUserRow->WDT_Enable       = 0x0;
    pUserRow->WDT_AlwaysOn     = 0x0;
    pUserRow->WDT_Period       = 0xB;
    pUserRow->WDT_Window       = 0xB;
    pUserRow->WDT_EWOFFSET     = 0xB;
    pUserRow->WDT_WEN          = 0x0;
    pUserRow->BOD33_Hysteresis = 0x0;
    pUserRow->BOD12_Hysteresis = 0x0;
    pUserRow->Reserved_4       = 0x1F;
    pUserRow->LOCK             = 0xFFFF;
}
