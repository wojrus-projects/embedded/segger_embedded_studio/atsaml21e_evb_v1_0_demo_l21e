//
// Peripheral Driver Low Layer (MCLK module).
//

#ifndef LL_MCLK_INCLUDED
#define LL_MCLK_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif


enum LL_MCLK_CPU_Clock_Division
{
    LL_MCLK_CPU_Clock_Division_DIV1   = 0x01,
    LL_MCLK_CPU_Clock_Division_DIV2   = 0x02,
    LL_MCLK_CPU_Clock_Division_DIV4   = 0x04,
    LL_MCLK_CPU_Clock_Division_DIV8   = 0x08,
    LL_MCLK_CPU_Clock_Division_DIV16  = 0x10,
    LL_MCLK_CPU_Clock_Division_DIV32  = 0x20,
    LL_MCLK_CPU_Clock_Division_DIV64  = 0x40,
    LL_MCLK_CPU_Clock_Division_DIV128 = 0x80
};


enum LL_MCLK_Low_Power_Clock_Division
{
    LL_MCLK_Low_Power_Clock_Division_DIV1   = 0x01,
    LL_MCLK_Low_Power_Clock_Division_DIV2   = 0x02,
    LL_MCLK_Low_Power_Clock_Division_DIV4   = 0x04,
    LL_MCLK_Low_Power_Clock_Division_DIV8   = 0x08,
    LL_MCLK_Low_Power_Clock_Division_DIV16  = 0x10,
    LL_MCLK_Low_Power_Clock_Division_DIV32  = 0x20,
    LL_MCLK_Low_Power_Clock_Division_DIV64  = 0x40,
    LL_MCLK_Low_Power_Clock_Division_DIV128 = 0x80
};


enum LL_MCLK_Backup_Clock_Division
{
    LL_MCLK_Backup_Clock_Division_DIV1   = 0x01,
    LL_MCLK_Backup_Clock_Division_DIV2   = 0x02,
    LL_MCLK_Backup_Clock_Division_DIV4   = 0x04,
    LL_MCLK_Backup_Clock_Division_DIV8   = 0x08,
    LL_MCLK_Backup_Clock_Division_DIV16  = 0x10,
    LL_MCLK_Backup_Clock_Division_DIV32  = 0x20,
    LL_MCLK_Backup_Clock_Division_DIV64  = 0x40,
    LL_MCLK_Backup_Clock_Division_DIV128 = 0x80
};


void LL_MCLK_InitializeClocks(void);
void LL_MCLK_SetClockDivision(enum LL_MCLK_CPU_Clock_Division cpuClock, enum LL_MCLK_Low_Power_Clock_Division lowPowerClock, enum LL_MCLK_Backup_Clock_Division backupClock);


#ifdef __cplusplus
}
#endif

#endif // LL_MCLK_INCLUDED
