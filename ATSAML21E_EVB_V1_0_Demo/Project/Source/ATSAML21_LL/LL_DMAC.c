#include <sam.h>
#include "LL_DMAC.h"


void LL_DMAC_Reset(void)
{
    LL_DMAC_Enable(0);

    DMAC->CTRL.bit.SWRST = 1;

    while (DMAC->CTRL.bit.SWRST == 1)
    {
    }
}


void LL_DMAC_Enable(uint32_t enable)
{
    DMAC_CTRL_Type ctrl;

    ctrl.reg = 0;
    ctrl.bit.SWRST = 0;
    ctrl.bit.DMAENABLE = (enable != 0);
    ctrl.bit.CRCENABLE = 0;
    ctrl.bit.LVLEN0 = 1;
    ctrl.bit.LVLEN1 = 1;
    ctrl.bit.LVLEN2 = 1;
    ctrl.bit.LVLEN3 = 1;

    DMAC->CTRL = ctrl;
}


void LL_DMAC_SetDebugControl(uint32_t runInDebugHaltedState)
{
    DMAC->DBGCTRL.reg = (runInDebugHaltedState != 0) << DMAC_DBGCTRL_DBGRUN_Pos;
}


void LL_DMAC_SetDescriptorMemoryBaseAddress(DmacDescriptor *pBaseAddress)
{
    DMAC->BASEADDR.reg = (uint32_t)pBaseAddress;
}


void LL_DMAC_SetWriteBackMemoryBaseAddress(DmacDescriptor *pBaseAddress)
{
    DMAC->WRBADDR.reg = (uint32_t)pBaseAddress;
}


void LL_DMAC_SetArbitrationMode(enum LL_DMAC_ArbitrationMode level0, enum LL_DMAC_ArbitrationMode level1, enum LL_DMAC_ArbitrationMode level2, enum LL_DMAC_ArbitrationMode level3)
{
    DMAC_PRICTRL0_Type prictrl0;

    prictrl0.reg = 0;
    prictrl0.bit.RRLVLEN0 = level0 & 0x1;
    prictrl0.bit.RRLVLEN1 = level1 & 0x1;
    prictrl0.bit.RRLVLEN2 = level2 & 0x1;
    prictrl0.bit.RRLVLEN3 = level3 & 0x1;

    DMAC->PRICTRL0 = prictrl0;
}


void LL_DMAC_SoftwareTrigger(uint32_t channelBitmap)
{
    DMAC->SWTRIGCTRL.reg = channelBitmap & 0xFFFF;
}


uint32_t LL_DMAC_GetBusyChannels(void)
{
    return DMAC->BUSYCH.vec.BUSYCH;
}


uint32_t LL_DMAC_GetPendingChannels(void)
{
    return DMAC->PENDCH.vec.PENDCH;
}


uint32_t LL_DMAC_GetInterruptStatus(void)
{
    return DMAC->INTSTATUS.vec.CHINT;
}


void LL_DMAC_Channel_Reset(uint32_t channelIndex)
{
    LL_DMAC_Channel_Enable(channelIndex, 0, 0);

    DMAC->CHID.reg = channelIndex;

    DMAC->CHCTRLA.bit.SWRST = 1;

    while (DMAC->CHCTRLA.bit.SWRST == 1)
    {
    }
}


void LL_DMAC_Channel_Enable(uint32_t channelIndex, uint32_t enable, uint32_t runInStandby)
{
    DMAC->CHID.reg = channelIndex;

    DMAC->CHCTRLA.reg = ((enable != 0) << DMAC_CHCTRLA_ENABLE_Pos) | ((runInStandby != 0) << DMAC_CHCTRLA_RUNSTDBY_Pos);
}


void LL_DMAC_Channel_SetCommand(uint32_t channelIndex, enum LL_DMAC_ChannelCommand command)
{
    DMAC->CHID.reg = channelIndex;

    DMAC_CHCTRLB_Type chctrlb = DMAC->CHCTRLB;

    chctrlb.bit.CMD = command & 0x3;

    DMAC->CHCTRLB = chctrlb;
}


void LL_DMAC_Channel_SetTrigger(uint32_t channelIndex, enum LL_DMAC_ChannelTriggerSource triggerSource, enum LL_DMAC_ChannelTriggerAction triggerAction)
{
    DMAC->CHID.reg = channelIndex;

    DMAC_CHCTRLB_Type chctrlb = DMAC->CHCTRLB;

    chctrlb.bit.TRIGSRC = triggerSource & 0x3F;
    chctrlb.bit.TRIGACT = triggerAction & 0x3;

    DMAC->CHCTRLB = chctrlb;
}


void LL_DMAC_Channel_SetEvent(uint32_t channelIndex, uint32_t eventOutputEnable, uint32_t eventInputEnable, enum LL_DMAC_ChannelEventAction eventInputAction)
{
    if (channelIndex > 7)
    {
        return;
    }

    DMAC->CHID.reg = channelIndex;

    DMAC_CHCTRLB_Type chctrlb = DMAC->CHCTRLB;

    chctrlb.bit.EVOE = (eventOutputEnable != 0);
    chctrlb.bit.EVIE = (eventInputEnable != 0);
    chctrlb.bit.EVACT = eventInputAction & 0x7;

    DMAC->CHCTRLB = chctrlb;
}


void LL_DMAC_Channel_ArbitrationLevel(uint32_t channelIndex, enum LL_DMAC_ArbitrationLevel arbitrationLevel)
{
    DMAC->CHID.reg = channelIndex;

    DMAC_CHCTRLB_Type chctrlb = DMAC->CHCTRLB;

    chctrlb.bit.LVL = arbitrationLevel & 0x3;

    DMAC->CHCTRLB = chctrlb;
}


enum LL_DMAC_ChannelStatus LL_DMAC_Channel_GetStatus(uint32_t channelIndex)
{
    DMAC->CHID.reg = channelIndex;

    return DMAC->CHSTATUS.reg;
}


void LL_DMAC_Channel_InterruptEnableClear(uint32_t channelIndex, enum LL_DMAC_ChannelInterruptFlag interruptFlag)
{
    DMAC->CHID.reg = channelIndex;

    DMAC->CHINTENCLR.reg = interruptFlag;
}


void LL_DMAC_Channel_InterruptEnableSet(uint32_t channelIndex, enum LL_DMAC_ChannelInterruptFlag interruptFlag)
{
    DMAC->CHID.reg = channelIndex;

    DMAC->CHINTENSET.reg = interruptFlag;
}


enum LL_DMAC_ChannelInterruptFlag LL_DMAC_Channel_GetInterruptFlag(uint32_t channelIndex)
{
    DMAC->CHID.reg = channelIndex;

    return (DMAC->CHINTFLAG.reg & 0x7);
}


void LL_DMAC_Channel_ClearInterruptFlag(uint32_t channelIndex, enum LL_DMAC_ChannelInterruptFlag interruptFlag)
{
    DMAC->CHID.reg = channelIndex;

    DMAC->CHINTFLAG.reg = interruptFlag & 0x7;
}


void LL_DMAC_Descriptor_Initialize(DmacDescriptor *pDescriptor,
                                   enum LL_DMAC_TransferBeatSize beatSize,
                                   uint32_t sourceAddressIncrementEnable,
                                   uint32_t destinationAddressIncrementEnable,
                                   enum LL_DMAC_TransferStepSelection stepSelection,
                                   enum LL_DMAC_TransferStepSize addressIncrementStepSize,
                                   enum LL_DMAC_TransferEventOutput eventOutputSelection,
                                   enum LL_DMAC_TransferBlockAction blockAction)
{
    pDescriptor->BTCTRL.bit.BEATSIZE = beatSize & 0x3;
    pDescriptor->BTCTRL.bit.SRCINC = (sourceAddressIncrementEnable != 0);
    pDescriptor->BTCTRL.bit.DSTINC = (destinationAddressIncrementEnable != 0);
    pDescriptor->BTCTRL.bit.STEPSEL = stepSelection & 0x1;
    pDescriptor->BTCTRL.bit.STEPSIZE = addressIncrementStepSize & 0x7;
    pDescriptor->BTCTRL.bit.EVOSEL = eventOutputSelection & 0x3;
    pDescriptor->BTCTRL.bit.BLOCKACT = blockAction & 0x3;
}


void LL_DMAC_Descriptor_SetBlockTransferCount(DmacDescriptor *pDescriptor, uint32_t blockTransferCount)
{
    pDescriptor->BTCNT.reg = blockTransferCount & 0xFFFF;
}


void LL_DMAC_Descriptor_SetBlockTransferAddress(DmacDescriptor *pDescriptor, volatile void *pAddressSource, volatile void *pAddressDestination)
{
    pDescriptor->SRCADDR.reg = (uint32_t)pAddressSource;
    pDescriptor->DSTADDR.reg = (uint32_t)pAddressDestination;
}


void LL_DMAC_Descriptor_SetNextDescriptorAddress(DmacDescriptor *pDescriptor, DmacDescriptor *pNextDescriptorAddress)
{
    pDescriptor->DESCADDR.reg = (uint32_t)pNextDescriptorAddress;
}


void LL_DMAC_Descriptor_SetValid(DmacDescriptor *pDescriptor, uint32_t valid)
{
    pDescriptor->BTCTRL.bit.VALID = (valid != 0);
}
