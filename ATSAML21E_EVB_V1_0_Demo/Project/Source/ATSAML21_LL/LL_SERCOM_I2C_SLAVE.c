#include <sam.h>
#include "LL_SERCOM_I2C_SLAVE.h"


static Sercom* const SERCOM_Instances[SERCOM_INST_NUM] = SERCOM_INSTS;


void LL_SERCOM_I2C_SLAVE_Reset(uint32_t sercomIndex)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CS.CTRLA.bit.SWRST = 1;

    while (pSercom->I2CS.SYNCBUSY.bit.SWRST == 1)
    {
    }
}


void LL_SERCOM_I2C_SLAVE_Enable(uint32_t sercomIndex, uint32_t enable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CS.CTRLA.bit.ENABLE = (enable != 0);

    while (pSercom->I2CS.SYNCBUSY.bit.ENABLE == 1)
    {
    }
}


void LL_SERCOM_I2C_SLAVE_SetRunInStandby(uint32_t sercomIndex, uint32_t runInStandby)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CS.CTRLA.bit.RUNSTDBY = (runInStandby != 0);
}


void LL_SERCOM_I2C_SLAVE_EnableFourWireMode(uint32_t sercomIndex, uint32_t enable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CS.CTRLA.bit.PINOUT = (enable != 0);
}


void LL_SERCOM_I2C_SLAVE_SetMode(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_Mode mode)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CS.CTRLA.bit.MODE = mode & 0x7;
}


void LL_SERCOM_I2C_SLAVE_SetSpeed(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_Speed speed)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CS.CTRLA.bit.SPEED = speed & 0x3;
}


void LL_SERCOM_I2C_SLAVE_SetClockStretchMode(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_ClockStretchMode mode)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CS.CTRLA.bit.SCLSM = mode & 0x1;
}


void LL_SERCOM_I2C_SLAVE_SetTimings(uint32_t sercomIndex,
                                    enum LL_SERCOM_I2C_SLAVE_SDAHoldTime sdaHoldTime,
                                    uint32_t slaveSCLLowExtendTimeOutEnable,
                                    uint32_t sclLowTimeOutEnable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    SERCOM_I2CS_CTRLA_Type ctrla = pSercom->I2CS.CTRLA;

    ctrla.bit.SDAHOLD = sdaHoldTime & 0x3;
    ctrla.bit.SEXTTOEN = (slaveSCLLowExtendTimeOutEnable != 0);
    ctrla.bit.LOWTOUTEN = (sclLowTimeOutEnable != 0);

    pSercom->I2CS.CTRLA = ctrla;
}


void LL_SERCOM_I2C_SLAVE_SetAcknowledgeAction(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_AcknowledgeAction action)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CS.CTRLB.bit.ACKACT = action & 0x1;
}


void LL_SERCOM_I2C_SLAVE_SetAddress(uint32_t sercomIndex, uint32_t generalCallAddressEnable, uint32_t address, uint32_t addressMask, uint32_t tenBitAddressingEnable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    SERCOM_I2CS_ADDR_Type addrReg;

    addrReg.reg = 0;
    addrReg.bit.GENCEN = (generalCallAddressEnable != 0);
    addrReg.bit.ADDR = address & 0x3FF;
    addrReg.bit.ADDRMASK = addressMask & 0x3FF;
    addrReg.bit.TENBITEN = (tenBitAddressingEnable != 0);

    pSercom->I2CS.ADDR = addrReg;
}


void LL_SERCOM_I2C_SLAVE_SetAddressMode(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_AddressMode addressMode)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CS.CTRLB.bit.AMODE = addressMode & 0x3;
}


void LL_SERCOM_I2C_SLAVE_EnableSmartMode(uint32_t sercomIndex, uint32_t enable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CS.CTRLB.bit.SMEN = (enable != 0);
}


void LL_SERCOM_I2C_SLAVE_EnablePMBusGroupCommand(uint32_t sercomIndex, uint32_t enable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CS.CTRLB.bit.GCMD = (enable != 0);
}


void LL_SERCOM_I2C_SLAVE_EnableAutomaticAcknowledge(uint32_t sercomIndex, uint32_t enable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CS.CTRLB.bit.AACKEN = (enable != 0);
}


void LL_SERCOM_I2C_SLAVE_TriggerCommand(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_Command command)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CS.CTRLB.bit.CMD = command & 0x3;
}


enum LL_SERCOM_I2C_SLAVE_Status LL_SERCOM_I2C_SLAVE_GetStatus(uint32_t sercomIndex)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    return pSercom->I2CS.STATUS.reg;
}


void LL_SERCOM_I2C_SLAVE_ClearStatus(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_Status status)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CS.STATUS.reg = status;
}


uint8_t LL_SERCOM_I2C_SLAVE_ReadData(uint32_t sercomIndex)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    return (uint8_t)pSercom->I2CS.DATA.reg;
}


void LL_SERCOM_I2C_SLAVE_WriteData(uint32_t sercomIndex, uint8_t dataTx)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CS.DATA.reg = (uint16_t)dataTx;
}


void LL_SERCOM_I2C_SLAVE_InterruptEnableClear(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_InterruptFlag interruptFlag)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CS.INTENCLR.reg = interruptFlag;
}


void LL_SERCOM_I2C_SLAVE_InterruptEnableSet(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_InterruptFlag interruptFlag)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CS.INTENSET.reg = interruptFlag;
}


enum LL_SERCOM_I2C_SLAVE_InterruptFlag LL_SERCOM_I2C_SLAVE_GetInterruptFlag(uint32_t sercomIndex)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    return pSercom->I2CS.INTFLAG.reg;
}


void LL_SERCOM_I2C_SLAVE_ClearInterruptFlag(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_InterruptFlag interruptFlag)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CS.INTFLAG.reg = interruptFlag;
}
