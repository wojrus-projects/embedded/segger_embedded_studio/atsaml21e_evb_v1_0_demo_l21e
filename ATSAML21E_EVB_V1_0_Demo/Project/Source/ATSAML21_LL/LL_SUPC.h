//
// Peripheral Driver Low Layer (SUPC module).
//

#ifndef LL_SUPC_INCLUDED
#define LL_SUPC_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


enum LL_SUPC_VoltageReference
{
    LL_SUPC_VoltageReference_1_0V  = 0x0,
    LL_SUPC_VoltageReference_1_1V  = 0x1,
    LL_SUPC_VoltageReference_1_2V  = 0x2,
    LL_SUPC_VoltageReference_1_25V = 0x3,
    LL_SUPC_VoltageReference_2_0V  = 0x4,
    LL_SUPC_VoltageReference_2_2V  = 0x5,
    LL_SUPC_VoltageReference_2_4V  = 0x6,
    LL_SUPC_VoltageReference_2_5V  = 0x7
};


enum LL_SUPC_Status
{
    LL_SUPC_Status_BOD33RDY = 1 << 0,
    LL_SUPC_Status_BOD33DET = 1 << 1,
    LL_SUPC_Status_B33SRDY  = 1 << 2,
    LL_SUPC_Status_VREGRDY  = 1 << 8,
    LL_SUPC_Status_APWSRDY  = 1 << 9,
    LL_SUPC_Status_VCORERDY = 1 << 10,
    LL_SUPC_Status_BBPS     = 1 << 11
};


enum LL_SUPC_VREG
{
    LL_SUPC_VREG_LDO           = 0,
    LL_SUPC_VREG_BuckConverter = 1
};


enum LL_SUPC_VREG_LowPowerModeEfficiency
{
    LL_SUPC_VREG_LowPowerModeEfficiency_Default = 0,
    LL_SUPC_VREG_LowPowerModeEfficiency_Highest = 1
};


enum LL_SUPC_VREG_PowerModeInStandby
{
    LL_SUPC_VREG_PowerModeInStandby_LowPower = 0,
    LL_SUPC_VREG_PowerModeInStandby_Normal   = 1
};


enum LL_SUPC_VREG_PerformanceLevelInStandby
{
    LL_SUPC_VREG_PerformanceLevelInStandby_Current = 0,
    LL_SUPC_VREG_PerformanceLevelInStandby_PL0     = 1
};


enum LL_SUPC_BOD33_Voltage
{
    LL_SUPC_BOD33_Voltage_VDD  = 0,
    LL_SUPC_BOD33_Voltage_VBAT = 1
};


enum LL_SUPC_BOD33_Action
{
    LL_SUPC_BOD33_Action_NONE  = 0x0,
    LL_SUPC_BOD33_Action_RESET = 0x1,
    LL_SUPC_BOD33_Action_INT   = 0x2,
    LL_SUPC_BOD33_Action_BKUP  = 0x3
};


enum LL_SUPC_BOD33_Prescaler
{
    LL_SUPC_BOD33_Prescaler_DIV2     = 0x0,
    LL_SUPC_BOD33_Prescaler_DIV4     = 0x1,
    LL_SUPC_BOD33_Prescaler_DIV8     = 0x2,
    LL_SUPC_BOD33_Prescaler_DIV16    = 0x3,
    LL_SUPC_BOD33_Prescaler_DIV32    = 0x4,
    LL_SUPC_BOD33_Prescaler_DIV64    = 0x5,
    LL_SUPC_BOD33_Prescaler_DIV128   = 0x6,
    LL_SUPC_BOD33_Prescaler_DIV256   = 0x7,
    LL_SUPC_BOD33_Prescaler_DIV512   = 0x8,
    LL_SUPC_BOD33_Prescaler_DIV1024  = 0x9,
    LL_SUPC_BOD33_Prescaler_DIV2048  = 0xA,
    LL_SUPC_BOD33_Prescaler_DIV4096  = 0xB,
    LL_SUPC_BOD33_Prescaler_DIV8192  = 0xC,
    LL_SUPC_BOD33_Prescaler_DIV16384 = 0xD,
    LL_SUPC_BOD33_Prescaler_DIV32768 = 0xE,
    LL_SUPC_BOD33_Prescaler_DIV65536 = 0xF
};


enum LL_SUPC_BOD33_StandbyConfiguration
{
    LL_SUPC_BOD33_StandbyConfiguration_Continuous = 0,
    LL_SUPC_BOD33_StandbyConfiguration_Sampling   = 1
};


enum LL_SUPC_BOD33_BackupConfiguration
{
    LL_SUPC_BOD33_BackupConfiguration_Disabled = 0,
    LL_SUPC_BOD33_BackupConfiguration_Sampling = 1
};


enum LL_SUPC_BOD33_ActiveConfiguration
{
    LL_SUPC_BOD33_ActiveConfiguration_Continuous = 0,
    LL_SUPC_BOD33_ActiveConfiguration_Sampling   = 1
};


enum LL_SUPC_InterruptFlag
{
    LL_SUPC_InterruptFlag_BOD33RDY = 1 << 0,
    LL_SUPC_InterruptFlag_BOD33DET = 1 << 1,
    LL_SUPC_InterruptFlag_B33SRDY  = 1 << 2,
    LL_SUPC_InterruptFlag_VREGRDY  = 1 << 8,
    LL_SUPC_InterruptFlag_APWSRDY  = 1 << 9,
    LL_SUPC_InterruptFlag_VCORERDY = 1 << 10
};


void LL_SUPC_InitializeVoltageReference(uint32_t outputEnable, uint32_t runInStandby, uint32_t onDemand, enum LL_SUPC_VoltageReference voltage);
void LL_SUPC_InitializeStandbySleepMode(enum LL_SUPC_VREG_PowerModeInStandby vregMode, enum LL_SUPC_VREG_PerformanceLevelInStandby vregPerformanceLevel);
enum LL_SUPC_Status LL_SUPC_GetStatus(void);

void LL_SUPC_VREG_Enable(uint32_t state);
void LL_SUPC_VREG_SelectType(enum LL_SUPC_VREG vregType);
void LL_SUPC_VREG_SetLowPowerModeEfficiency(enum LL_SUPC_VREG_LowPowerModeEfficiency vregEfficiency);

void LL_SUPC_BOD33_Enable(uint32_t state);
void LL_SUPC_BOD33_SetMonitoredVoltage(enum LL_SUPC_BOD33_Voltage voltage);
void LL_SUPC_BOD33_SetHysteresis(uint32_t state);
void LL_SUPC_BOD33_SetAction(enum LL_SUPC_BOD33_Action action);
void LL_SUPC_BOD33_SetThresholdLevel(uint32_t vddLevel, uint32_t backupLevel);
void LL_SUPC_BOD33_SetSamplingPrescaler(enum LL_SUPC_BOD33_Prescaler prescaler);
void LL_SUPC_BOD33_SetSleepConfiguration(uint32_t standbyEnable, enum LL_SUPC_BOD33_StandbyConfiguration standbyConfiguration, enum LL_SUPC_BOD33_BackupConfiguration backupConfiguration, enum LL_SUPC_BOD33_ActiveConfiguration activeConfiguration);

void LL_SUPC_InterruptEnableClear(uint32_t sercomIndex, enum LL_SUPC_InterruptFlag interruptFlag);
void LL_SUPC_InterruptEnableSet(uint32_t sercomIndex, enum LL_SUPC_InterruptFlag interruptFlag);
enum LL_SUPC_InterruptFlag LL_SUPC_GetInterruptFlag(uint32_t sercomIndex);
void LL_SUPC_ClearInterruptFlag(uint32_t sercomIndex, enum LL_SUPC_InterruptFlag interruptFlag);


#ifdef __cplusplus
}
#endif

#endif // LL_SUPC_INCLUDED
