#include <sam.h>
#include "LL_PM.h"


void LL_PM_SetPerformanceLevel(uint32_t disableAutomaticLevelSelection, enum LL_PM_PerformanceLevel level)
{
    PM_PLCFG_Type plcfg;

    plcfg.reg = 0;
    plcfg.bit.PLDIS = (disableAutomaticLevelSelection != 0);
    plcfg.bit.PLSEL = level & 0x3;

    PM->PLCFG = plcfg;
}


void LL_PM_SetSleepMode(enum LL_PM_SleepMode mode)
{
    PM->SLEEPCFG.reg = PM_SLEEPCFG_SLEEPMODE(mode);

    while (PM->SLEEPCFG.reg != PM_SLEEPCFG_SLEEPMODE(mode))
    {
    }
}
