//
// Peripheral Driver Low Layer (CCL module).
//

#ifndef LL_CCL_INCLUDED
#define LL_CCL_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


// Number of LUTs and sequential Logic
#define LL_CCL_LUT_NUM      CCL_LUT_NUM
#define LL_CCL_SEQ_NUM      CCL_SEQ_NUM


enum LL_CCL_Sequential
{
    LL_CCL_Sequential_DISABLE = 0x0,
    LL_CCL_Sequential_DFF     = 0x1,
    LL_CCL_Sequential_JK      = 0x2,
    LL_CCL_Sequential_LATCH   = 0x3,
    LL_CCL_Sequential_RS      = 0x4
};


enum LL_CCL_LUT_Input
{
    LL_CCL_LUT_Input_MASK     = 0x0,
    LL_CCL_LUT_Input_FEEDBACK = 0x1,
    LL_CCL_LUT_Input_LINK     = 0x2,
    LL_CCL_LUT_Input_EVENT    = 0x3,
    LL_CCL_LUT_Input_IO       = 0x4,
    LL_CCL_LUT_Input_AC       = 0x5,
    LL_CCL_LUT_Input_TC       = 0x6,
    LL_CCL_LUT_Input_ALTTC    = 0x7,
    LL_CCL_LUT_Input_SERCOM   = 0x9
};


enum LL_CCL_LUT_Filter
{
    LL_CCL_LUT_Filter_DISABLE = 0x0,
    LL_CCL_LUT_Filter_SYNCH   = 0x1,
    LL_CCL_LUT_Filter_FILTER  = 0x2
};


void LL_CCL_Reset(void);
void LL_CCL_Enable(uint32_t enable, uint32_t runInStandby);
void LL_CCL_SelectSequential(uint32_t seqIndex, enum LL_CCL_Sequential sequentialMode);
void LL_CCL_LUT_Enable(uint32_t lutIndex, uint32_t enable);
void LL_CCL_LUT_EnableEvent(uint32_t lutIndex, uint32_t invertedEventInputEnable, uint32_t eventInputEnable, uint32_t eventOutputEnable);
void LL_CCL_LUT_SetTruthTable(uint32_t lutIndex, uint8_t truthTable);
void LL_CCL_LUT_SelectInput(uint32_t lutIndex, enum LL_CCL_LUT_Input input0, enum LL_CCL_LUT_Input input1, enum LL_CCL_LUT_Input input2);
void LL_CCL_LUT_EnableEdgeDetection(uint32_t lutIndex, uint32_t enable);
void LL_CCL_LUT_SetFilter(uint32_t lutIndex, enum LL_CCL_LUT_Filter filter);


#ifdef __cplusplus
}
#endif

#endif // LL_CCL_INCLUDED
