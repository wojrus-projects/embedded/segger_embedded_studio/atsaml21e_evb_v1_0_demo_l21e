#include <sam.h>
#include "LL_AES.h"


void LL_AES_Reset(void)
{
    AES->CTRLA.bit.SWRST = 1;

    while (AES->CTRLA.bit.SWRST == 1)
    {
    }
}


void LL_AES_Enable(uint32_t enable)
{
    AES->CTRLA.bit.ENABLE = (enable != 0);
}
