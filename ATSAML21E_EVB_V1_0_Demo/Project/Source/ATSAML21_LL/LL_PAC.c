#include <sam.h>
#include "LL_PAC.h"


void LL_PAC_SetWriteControl(enum LL_PAC_ControlMode mode, uint32_t peripheralBridgeNumber, uint32_t peripheralIndex)
{
    PAC_WRCTRL_Type wrctrl;

    wrctrl.reg = 0;
    wrctrl.bit.KEY = mode & 0xFF;
    wrctrl.bit.PERID = 32 * peripheralBridgeNumber + peripheralIndex;

    PAC->WRCTRL = wrctrl;
}
