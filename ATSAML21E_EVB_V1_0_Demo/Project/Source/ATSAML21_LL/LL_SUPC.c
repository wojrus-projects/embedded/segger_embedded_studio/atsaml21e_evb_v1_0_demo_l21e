#include <sam.h>
#include "LL_SUPC.h"


void LL_SUPC_InitializeVoltageReference(uint32_t outputEnable, uint32_t runInStandby, uint32_t onDemand, enum LL_SUPC_VoltageReference voltage)
{
    SUPC_VREF_Type vref = SUPC->VREF;

    vref.bit.VREFOE = (outputEnable != 0);
    vref.bit.RUNSTDBY = (runInStandby != 0);
    vref.bit.ONDEMAND = (onDemand != 0);
    vref.bit.SEL = voltage & 0xF;

    SUPC->VREF = vref;
}


void LL_SUPC_InitializeStandbySleepMode(enum LL_SUPC_VREG_PowerModeInStandby vregMode, enum LL_SUPC_VREG_PerformanceLevelInStandby vregPerformanceLevel)
{
    SUPC_VREG_Type vreg = SUPC->VREG;

    vreg.bit.RUNSTDBY = vregMode & 0x1;
    vreg.bit.STDBYPL0 = vregPerformanceLevel & 0x1;

    SUPC->VREG = vreg;
}


enum LL_SUPC_Status LL_SUPC_GetStatus(void)
{
    return SUPC->STATUS.reg;
}


void LL_SUPC_VREG_Enable(uint32_t state)
{
    SUPC->VREG.bit.ENABLE = (state != 0);
}


void LL_SUPC_VREG_SelectType(enum LL_SUPC_VREG vregType)
{
    SUPC->VREG.bit.SEL = vregType & 0x1;
}


void LL_SUPC_VREG_SetLowPowerModeEfficiency(enum LL_SUPC_VREG_LowPowerModeEfficiency vregEfficiency)
{
    SUPC->VREG.bit.LPEFF = vregEfficiency & 0x1;
}


void LL_SUPC_BOD33_Enable(uint32_t state)
{
    SUPC->BOD33.bit.ENABLE = (state != 0);

    while (SUPC->STATUS.bit.B33SRDY == 0)
    {
    }
}


void LL_SUPC_BOD33_SetMonitoredVoltage(enum LL_SUPC_BOD33_Voltage voltage)
{
    SUPC->BOD33.bit.VMON = voltage & 0x1;
}


void LL_SUPC_BOD33_SetHysteresis(uint32_t state)
{
    SUPC->BOD33.bit.HYST = (state != 0);
}


void LL_SUPC_BOD33_SetAction(enum LL_SUPC_BOD33_Action action)
{
    SUPC->BOD33.bit.ACTION = action & 0x3;
}


void LL_SUPC_BOD33_SetThresholdLevel(uint32_t vddLevel, uint32_t backupLevel)
{
    SUPC_BOD33_Type bod33 = SUPC->BOD33;

    bod33.bit.LEVEL = vddLevel & 0x3F;
    bod33.bit.BKUPLEVEL = backupLevel & 0x3F;

    SUPC->BOD33 = bod33;
}


void LL_SUPC_BOD33_SetSamplingPrescaler(enum LL_SUPC_BOD33_Prescaler prescaler)
{
    SUPC->BOD33.bit.PSEL = prescaler & 0xF;
}


void LL_SUPC_BOD33_SetSleepConfiguration(uint32_t standbyEnable,
                                         enum LL_SUPC_BOD33_StandbyConfiguration standbyConfiguration,
                                         enum LL_SUPC_BOD33_BackupConfiguration backupConfiguration,
                                         enum LL_SUPC_BOD33_ActiveConfiguration activeConfiguration)
{
    SUPC_BOD33_Type bod33 = SUPC->BOD33;

    bod33.bit.RUNSTDBY = (standbyEnable != 0);
    bod33.bit.STDBYCFG = standbyConfiguration & 0x1;
    bod33.bit.RUNBKUP = backupConfiguration & 0x1;
    bod33.bit.ACTCFG = activeConfiguration & 0x1;

    SUPC->BOD33 = bod33;
}


void LL_SUPC_InterruptEnableClear(uint32_t sercomIndex, enum LL_SUPC_InterruptFlag interruptFlag)
{
    SUPC->INTENCLR.reg = interruptFlag;
}


void LL_SUPC_InterruptEnableSet(uint32_t sercomIndex, enum LL_SUPC_InterruptFlag interruptFlag)
{
    SUPC->INTENSET.reg = interruptFlag;
}


enum LL_SUPC_InterruptFlag LL_SUPC_GetInterruptFlag(uint32_t sercomIndex)
{
    return SUPC->INTFLAG.reg;
}


void LL_SUPC_ClearInterruptFlag(uint32_t sercomIndex, enum LL_SUPC_InterruptFlag interruptFlag)
{
    SUPC->INTFLAG.reg = interruptFlag;
}
