//
// Peripheral Driver Low Layer (AES module).
//

#ifndef LL_AES_INCLUDED
#define LL_AES_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


void LL_AES_Reset(void);
void LL_AES_Enable(uint32_t enable);


#ifdef __cplusplus
}
#endif

#endif // LL_AES_INCLUDED
