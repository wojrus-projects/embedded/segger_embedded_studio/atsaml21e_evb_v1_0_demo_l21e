//
// Peripheral Driver Low Layer (WDT module).
//

#ifndef LL_WDT_INCLUDED
#define LL_WDT_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


enum LL_WDT_TimeOutPeriod
{
    LL_WDT_TimeOutPeriod_CYC8     = 0x0,    // 8 ms
    LL_WDT_TimeOutPeriod_CYC16    = 0x1,    // 16 ms
    LL_WDT_TimeOutPeriod_CYC32    = 0x2,    // 31 ms
    LL_WDT_TimeOutPeriod_CYC64    = 0x3,    // 62 ms
    LL_WDT_TimeOutPeriod_CYC128   = 0x4,    // 125 ms
    LL_WDT_TimeOutPeriod_CYC256   = 0x5,    // 250 ms
    LL_WDT_TimeOutPeriod_CYC512   = 0x6,    // 500 ms
    LL_WDT_TimeOutPeriod_CYC1024  = 0x7,    // 1 s
    LL_WDT_TimeOutPeriod_CYC2048  = 0x8,    // 2 s
    LL_WDT_TimeOutPeriod_CYC4096  = 0x9,    // 4 s
    LL_WDT_TimeOutPeriod_CYC8192  = 0xA,    // 8 s
    LL_WDT_TimeOutPeriod_CYC16384 = 0xB     // 16 s
};


enum LL_WDT_WindowTimeOutPeriod
{
    LL_WDT_WindowTimeOutPeriod_CYC8     = 0x0,    // 8 ms
    LL_WDT_WindowTimeOutPeriod_CYC16    = 0x1,    // 16 ms
    LL_WDT_WindowTimeOutPeriod_CYC32    = 0x2,    // 31 ms
    LL_WDT_WindowTimeOutPeriod_CYC64    = 0x3,    // 62 ms
    LL_WDT_WindowTimeOutPeriod_CYC128   = 0x4,    // 125 ms
    LL_WDT_WindowTimeOutPeriod_CYC256   = 0x5,    // 250 ms
    LL_WDT_WindowTimeOutPeriod_CYC512   = 0x6,    // 500 ms
    LL_WDT_WindowTimeOutPeriod_CYC1024  = 0x7,    // 1 s
    LL_WDT_WindowTimeOutPeriod_CYC2048  = 0x8,    // 2 s
    LL_WDT_WindowTimeOutPeriod_CYC4096  = 0x9,    // 4 s
    LL_WDT_WindowTimeOutPeriod_CYC8192  = 0xA,    // 8 s
    LL_WDT_WindowTimeOutPeriod_CYC16384 = 0xB     // 16 s
};


enum LL_WDT_EarlyWarningTimeOffset
{
    LL_WDT_EarlyWarningTimeOffset_CYC8     = 0x0,    // 8 ms
    LL_WDT_EarlyWarningTimeOffset_CYC16    = 0x1,    // 16 ms
    LL_WDT_EarlyWarningTimeOffset_CYC32    = 0x2,    // 31 ms
    LL_WDT_EarlyWarningTimeOffset_CYC64    = 0x3,    // 62 ms
    LL_WDT_EarlyWarningTimeOffset_CYC128   = 0x4,    // 125 ms
    LL_WDT_EarlyWarningTimeOffset_CYC256   = 0x5,    // 250 ms
    LL_WDT_EarlyWarningTimeOffset_CYC512   = 0x6,    // 500 ms
    LL_WDT_EarlyWarningTimeOffset_CYC1024  = 0x7,    // 1 s
    LL_WDT_EarlyWarningTimeOffset_CYC2048  = 0x8,    // 2 s
    LL_WDT_EarlyWarningTimeOffset_CYC4096  = 0x9,    // 4 s
    LL_WDT_EarlyWarningTimeOffset_CYC8192  = 0xA,    // 8 s
    LL_WDT_EarlyWarningTimeOffset_CYC16384 = 0xB     // 16 s
};


void LL_WDT_SetConfiguration(enum LL_WDT_TimeOutPeriod timeOutPeriod, enum LL_WDT_WindowTimeOutPeriod windowModeTimeOutPeriod);
void LL_WDT_SetEarlyWarningInterruptTimeOffset(enum LL_WDT_EarlyWarningTimeOffset timeOffset);
void LL_WDT_Enable(uint32_t enable, uint32_t windowModeEnable, uint32_t alwaysOn);
uint32_t LL_WDT_IsEnabled(void);
void LL_WDT_Clear(void);

void LL_WDT_InterruptEnableClear(void);
void LL_WDT_InterruptEnableSet(void);
uint32_t LL_WDT_GetInterruptFlag(void);
void LL_WDT_ClearInterruptFlag(void);


#ifdef __cplusplus
}
#endif

#endif // LL_WDT_INCLUDED
