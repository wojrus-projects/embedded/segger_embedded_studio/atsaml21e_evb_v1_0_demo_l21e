//
// Peripheral Driver Low Layer (PAC module).
//

#ifndef LL_PAC_INCLUDED
#define LL_PAC_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


enum LL_PAC_ControlMode
{
    LL_PAC_ControlMode_OFF   = 0x0,
    LL_PAC_ControlMode_CLEAR = 0x1,
    LL_PAC_ControlMode_SET   = 0x2,
    LL_PAC_ControlMode_LOCK  = 0x3
};


void LL_PAC_SetWriteControl(enum LL_PAC_ControlMode mode, uint32_t peripheralBridgeNumber, uint32_t peripheralIndex);


#ifdef __cplusplus
}
#endif

#endif // LL_PAC_INCLUDED
