#include <sam.h>
#include "LL_GCLK.h"


void LL_GCLK_Reset(void)
{
    GCLK->CTRLA.bit.SWRST = 1;

    while (GCLK->SYNCBUSY.bit.SWRST == 1)
    {
    }
}


void LL_GCLK_EnableGenerator(uint32_t generatorIndex,
                             uint32_t enable,
                             enum LL_GCLK_ClockSource clockSource,
                             uint32_t divisionFactor,
                             uint32_t runInStandby,
                             uint32_t divideSelection,
                             uint32_t outputEnable,
                             uint32_t outputOffValue)
{
    GCLK_GENCTRL_Type genctrl = GCLK->GENCTRL[generatorIndex];

    genctrl.reg = 0;
    genctrl.bit.SRC = clockSource & 0x1F;
    genctrl.bit.GENEN = (enable != 0);
    genctrl.bit.IDC = 0;
    genctrl.bit.OOV = (outputOffValue != 0);
    genctrl.bit.OE = (outputEnable != 0);
    genctrl.bit.DIVSEL = (divideSelection != 0);
    genctrl.bit.RUNSTDBY = (runInStandby != 0);
    genctrl.bit.DIV = divisionFactor & 0xFFFF;

    GCLK->GENCTRL[generatorIndex] = genctrl;

    while (GCLK->SYNCBUSY.reg != 0)
    {
    }
}


void LL_GCLK_EnablePeripheralChannel(enum LL_GCLK_Channel channel, uint32_t generatorIndex, uint32_t enable)
{
    GCLK->PCHCTRL[channel].reg = GCLK_PCHCTRL_GEN(generatorIndex) | ((enable != 0) << GCLK_PCHCTRL_CHEN_Pos) | (0 << GCLK_PCHCTRL_WRTLOCK_Pos);
}
