#include <sam.h>
#include "LL_RSTC.h"


enum LL_RSTC_ResetCause LL_RSTC_GetResetCause(void)
{
    const RSTC_RCAUSE_Type rcause = RSTC->RCAUSE;

    enum LL_RSTC_ResetCause resetCause = 0;

    if (rcause.reg & RSTC_RCAUSE_POR)
    {
        resetCause = LL_RSTC_ResetCause_POR;
    }
    else if (rcause.reg & RSTC_RCAUSE_BOD12)
    {
        resetCause = LL_RSTC_ResetCause_BOD12;
    }
    else if (rcause.reg & RSTC_RCAUSE_BOD33)
    {
        resetCause = LL_RSTC_ResetCause_BOD33;
    }
    else if (rcause.reg & RSTC_RCAUSE_EXT)
    {
        resetCause = LL_RSTC_ResetCause_EXT;
    }
    else if (rcause.reg & RSTC_RCAUSE_WDT)
    {
        resetCause = LL_RSTC_ResetCause_WDT;
    }
    else if (rcause.reg & RSTC_RCAUSE_SYST)
    {
        resetCause = LL_RSTC_ResetCause_SYST;
    }
    else if (rcause.reg & RSTC_RCAUSE_BACKUP)
    {
        resetCause = LL_RSTC_ResetCause_BACKUP;
    }

    return resetCause;
}
