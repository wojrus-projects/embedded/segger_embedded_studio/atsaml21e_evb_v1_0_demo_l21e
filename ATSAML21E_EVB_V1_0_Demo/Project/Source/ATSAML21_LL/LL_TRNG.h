//
// Peripheral Driver Low Layer (TRNG module).
//

#ifndef LL_TRNG_INCLUDED
#define LL_TRNG_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


void LL_TRNG_Initialize(uint32_t enable, uint32_t runInSleep);
void LL_TRNG_EnableDataReadyEventOutput(uint32_t enable);
uint32_t LL_TRNG_GetData(void);

void LL_TRNG_InterruptEnableClear(void);
void LL_TRNG_InterruptEnableSet(void);
uint32_t LL_TRNG_GetAndClearInterruptFlag(void);


#ifdef __cplusplus
}
#endif

#endif // LL_TRNG_INCLUDED
