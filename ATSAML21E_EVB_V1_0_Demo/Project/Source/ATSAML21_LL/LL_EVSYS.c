#include <sam.h>
#include "LL_EVSYS.h"


void LL_EVSYS_Reset(void)
{
    EVSYS->CTRLA.reg = EVSYS_CTRLA_SWRST;
}


void LL_EVSYS_InitializeChannel(uint32_t channelIndex,
                                enum LL_EVSYS_EventGenerator eventGenerator,
                                enum LL_EVSYS_Path path,
                                enum LL_EVSYS_EdgeDetection edgeDetection,
                                uint32_t runInStandby,
                                uint32_t onDemand)
{
    EVSYS_CHANNEL_Type channel;

    channel.reg = 0;
    channel.bit.EVGEN = eventGenerator & 0x7F;
    channel.bit.PATH = path & 0x3;
    channel.bit.EDGSEL = edgeDetection & 0x3;
    channel.bit.RUNSTDBY = (runInStandby != 0);
    channel.bit.ONDEMAND = (onDemand != 0);

    EVSYS->CHANNEL[channelIndex] = channel;
}


void LL_EVSYS_AssignUserToChannel(enum LL_EVSYS_User userIndex, uint32_t channelIndex, uint32_t assign)
{
    if (assign != 0)
    {
        EVSYS->USER[userIndex].bit.CHANNEL = (channelIndex & 0x3F) + 1;
    }
    else
    {
        EVSYS->USER[userIndex].bit.CHANNEL = 0;
    }
}


void LL_EVSYS_TriggerSoftwareEvent(uint32_t eventChannelBitmap)
{
    EVSYS->SWEVT.reg = eventChannelBitmap;
}


uint32_t LL_EVSYS_GetStatusUserReady(void)
{
    return EVSYS->CHSTATUS.vec.USRRDY;
}


uint32_t LL_EVSYS_GetStatusChannelBusy(void)
{
    return EVSYS->CHSTATUS.vec.CHBUSY;
}


void LL_EVSYS_InterruptEnableClear(uint32_t overrunChannelBitmap, uint32_t eventDetectedChannelBitmap)
{
    EVSYS->INTENCLR.reg = EVSYS_INTENCLR_OVR(overrunChannelBitmap) | EVSYS_INTENCLR_EVD(eventDetectedChannelBitmap);
}


void LL_EVSYS_InterruptEnableSet(uint32_t overrunChannelBitmap, uint32_t eventDetectedChannelBitmap)
{
    EVSYS->INTENSET.reg = EVSYS_INTENSET_OVR(overrunChannelBitmap) | EVSYS_INTENSET_EVD(eventDetectedChannelBitmap);
}


void LL_EVSYS_GetInterruptFlag(uint32_t * pOverrunChannelBitmap, uint32_t * pEventDetectedChannelBitmap)
{
    *pOverrunChannelBitmap = (uint32_t)(EVSYS->INTFLAG.vec.OVR);
    *pEventDetectedChannelBitmap = (uint32_t)(EVSYS->INTFLAG.vec.EVD);
}


void LL_EVSYS_ClearInterruptFlag(uint32_t overrunChannelBitmap, uint32_t eventDetectedChannelBitmap)
{
    EVSYS->INTFLAG.reg = EVSYS_INTFLAG_OVR(overrunChannelBitmap) | EVSYS_INTFLAG_EVD(eventDetectedChannelBitmap);
}
