//
// Peripheral Driver Low Layer (SERCOM_I2CS module).
//

#ifndef LL_SERCOM_I2C_SLAVE_INCLUDED
#define LL_SERCOM_I2C_SLAVE_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


// Number of SERCOM-I2C-SLAVE modules
#define LL_SERCOM_I2C_SLAVE_NUM     SERCOM_INST_NUM


enum LL_SERCOM_I2C_SLAVE_Mode
{
    LL_SERCOM_I2C_SLAVE_Mode_Slave = 0x4
};


enum LL_SERCOM_I2C_SLAVE_Speed
{
    LL_SERCOM_I2C_SLAVE_Speed_Standard = 0x0,
    LL_SERCOM_I2C_SLAVE_Speed_Fast     = 0x1,
    LL_SERCOM_I2C_SLAVE_Speed_High     = 0x2
};


enum LL_SERCOM_I2C_SLAVE_ClockStretchMode
{
    LL_SERCOM_I2C_SLAVE_ClockStretchMode_Standard = 0x0,
    LL_SERCOM_I2C_SLAVE_ClockStretchMode_AfterACK = 0x1
};


enum LL_SERCOM_I2C_SLAVE_SDAHoldTime
{
    LL_SERCOM_I2C_SLAVE_SDAHoldTime_DIS   = 0x0,
    LL_SERCOM_I2C_SLAVE_SDAHoldTime_75NS  = 0x1,
    LL_SERCOM_I2C_SLAVE_SDAHoldTime_450NS = 0x2,
    LL_SERCOM_I2C_SLAVE_SDAHoldTime_600NS = 0x3
};


enum LL_SERCOM_I2C_SLAVE_AcknowledgeAction
{
    LL_SERCOM_I2C_SLAVE_AcknowledgeAction_SendACK  = 0x0,
    LL_SERCOM_I2C_SLAVE_AcknowledgeAction_SendNACK = 0x1
};


enum LL_SERCOM_I2C_SLAVE_Command
{
    LL_SERCOM_I2C_SLAVE_Command_DRDY_Complete  = 0x2,
    LL_SERCOM_I2C_SLAVE_Command_DRDY           = 0x3,
    LL_SERCOM_I2C_SLAVE_Command_AMATCH         = 0x3
};


enum LL_SERCOM_I2C_SLAVE_AddressMode
{
    LL_SERCOM_I2C_SLAVE_AddressMode_MASK    = 0x0,
    LL_SERCOM_I2C_SLAVE_AddressMode_2_ADDRS = 0x1,
    LL_SERCOM_I2C_SLAVE_AddressMode_RANGE   = 0x2
};


enum LL_SERCOM_I2C_SLAVE_Status
{
    LL_SERCOM_I2C_SLAVE_Status_BUSERR   = 1 << 0,
    LL_SERCOM_I2C_SLAVE_Status_COLL     = 1 << 1,
    LL_SERCOM_I2C_SLAVE_Status_RXNACK   = 1 << 2,
    LL_SERCOM_I2C_SLAVE_Status_DIR      = 1 << 3,
    LL_SERCOM_I2C_SLAVE_Status_SR       = 1 << 4,
    LL_SERCOM_I2C_SLAVE_Status_LOWTOUT  = 1 << 6,
    LL_SERCOM_I2C_SLAVE_Status_CLKHOLD  = 1 << 7,
    LL_SERCOM_I2C_SLAVE_Status_SEXTTOUT = 1 << 9,
    LL_SERCOM_I2C_SLAVE_Status_HS       = 1 << 10,
    LL_SERCOM_I2C_SLAVE_Status_LENERR   = 1 << 11
};


enum LL_SERCOM_I2C_SLAVE_InterruptFlag
{
    LL_SERCOM_I2C_SLAVE_InterruptFlag_PREC   = 1 << 0,
    LL_SERCOM_I2C_SLAVE_InterruptFlag_AMATCH = 1 << 1,
    LL_SERCOM_I2C_SLAVE_InterruptFlag_DRDY   = 1 << 2,
    LL_SERCOM_I2C_SLAVE_InterruptFlag_ERROR  = 1 << 7
};


void LL_SERCOM_I2C_SLAVE_Reset(uint32_t sercomIndex);
void LL_SERCOM_I2C_SLAVE_Enable(uint32_t sercomIndex, uint32_t enable);
void LL_SERCOM_I2C_SLAVE_SetRunInStandby(uint32_t sercomIndex, uint32_t runInStandby);

void LL_SERCOM_I2C_SLAVE_EnableFourWireMode(uint32_t sercomIndex, uint32_t enable);
void LL_SERCOM_I2C_SLAVE_SetMode(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_Mode mode);
void LL_SERCOM_I2C_SLAVE_SetSpeed(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_Speed speed);
void LL_SERCOM_I2C_SLAVE_SetClockStretchMode(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_ClockStretchMode mode);
void LL_SERCOM_I2C_SLAVE_SetTimings(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_SDAHoldTime sdaHoldTime, uint32_t slaveSCLLowExtendTimeOutEnable, uint32_t sclLowTimeOutEnable);
void LL_SERCOM_I2C_SLAVE_SetAcknowledgeAction(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_AcknowledgeAction action);
void LL_SERCOM_I2C_SLAVE_SetAddress(uint32_t sercomIndex, uint32_t generalCallAddressEnable, uint32_t address, uint32_t addressMask, uint32_t tenBitAddressingEnable);
void LL_SERCOM_I2C_SLAVE_SetAddressMode(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_AddressMode addressMode);
void LL_SERCOM_I2C_SLAVE_EnableSmartMode(uint32_t sercomIndex, uint32_t enable);
void LL_SERCOM_I2C_SLAVE_EnablePMBusGroupCommand(uint32_t sercomIndex, uint32_t enable);
void LL_SERCOM_I2C_SLAVE_EnableAutomaticAcknowledge(uint32_t sercomIndex, uint32_t enable);
void LL_SERCOM_I2C_SLAVE_TriggerCommand(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_Command command);

enum LL_SERCOM_I2C_SLAVE_Status LL_SERCOM_I2C_SLAVE_GetStatus(uint32_t sercomIndex);
void LL_SERCOM_I2C_SLAVE_ClearStatus(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_Status status);

uint8_t LL_SERCOM_I2C_SLAVE_ReadData(uint32_t sercomIndex);
void LL_SERCOM_I2C_SLAVE_WriteData(uint32_t sercomIndex, uint8_t dataTx);

void LL_SERCOM_I2C_SLAVE_InterruptEnableClear(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_InterruptFlag interruptFlag);
void LL_SERCOM_I2C_SLAVE_InterruptEnableSet(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_InterruptFlag interruptFlag);
enum LL_SERCOM_I2C_SLAVE_InterruptFlag LL_SERCOM_I2C_SLAVE_GetInterruptFlag(uint32_t sercomIndex);
void LL_SERCOM_I2C_SLAVE_ClearInterruptFlag(uint32_t sercomIndex, enum LL_SERCOM_I2C_SLAVE_InterruptFlag interruptFlag);


#ifdef __cplusplus
}
#endif

#endif // LL_SERCOM_I2C_SLAVE_INCLUDED
