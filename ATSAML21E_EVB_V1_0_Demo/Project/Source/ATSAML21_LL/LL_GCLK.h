//
// Peripheral Driver Low Layer (GCLK module).
//

#ifndef LL_GCLK_INCLUDED
#define LL_GCLK_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


// Number of Generic Clock Generators
#define LL_GCLK_GEN_NUM        GCLK_GEN_NUM


enum LL_GCLK_ClockSource
{
    LL_GCLK_ClockSource_XOSC      = 0x00,
    LL_GCLK_ClockSource_GCLK_IN   = 0x01,
    LL_GCLK_ClockSource_GCLK_GEN1 = 0x02,
    LL_GCLK_ClockSource_OSCULP32K = 0x03,
    LL_GCLK_ClockSource_OSC32K    = 0x04,
    LL_GCLK_ClockSource_XOSC32K   = 0x05,
    LL_GCLK_ClockSource_OSC16M    = 0x06,
    LL_GCLK_ClockSource_DFLL48M   = 0x07,
    LL_GCLK_ClockSource_DPLL96M   = 0x08
};


enum LL_GCLK_Channel
{
    LL_GCLK_Channel_DFLL48M_REF       = 0,
    LL_GCLK_Channel_DPLL              = 1,
    LL_GCLK_Channel_DPLL_32K          = 2,
    LL_GCLK_Channel_EIC               = 3,
    LL_GCLK_Channel_USB               = 4,
    LL_GCLK_Channel_EVSYS_CHANNEL_0   = 5,
    LL_GCLK_Channel_EVSYS_CHANNEL_1   = 6,
    LL_GCLK_Channel_EVSYS_CHANNEL_2   = 7,
    LL_GCLK_Channel_EVSYS_CHANNEL_3   = 8,
    LL_GCLK_Channel_EVSYS_CHANNEL_4   = 9,
    LL_GCLK_Channel_EVSYS_CHANNEL_5   = 10,
    LL_GCLK_Channel_EVSYS_CHANNEL_6   = 11,
    LL_GCLK_Channel_EVSYS_CHANNEL_7   = 12,
    LL_GCLK_Channel_EVSYS_CHANNEL_8   = 13,
    LL_GCLK_Channel_EVSYS_CHANNEL_9   = 14,
    LL_GCLK_Channel_EVSYS_CHANNEL_10  = 15,
    LL_GCLK_Channel_EVSYS_CHANNEL_11  = 16,
    LL_GCLK_Channel_SERCOM_01234_SLOW = 17,
    LL_GCLK_Channel_SERCOM0_CORE      = 18,
    LL_GCLK_Channel_SERCOM1_CORE      = 19,
    LL_GCLK_Channel_SERCOM2_CORE      = 20,
    LL_GCLK_Channel_SERCOM3_CORE      = 21,
    LL_GCLK_Channel_SERCOM4_CORE      = 22,
    LL_GCLK_Channel_SERCOM5_SLOW      = 23,
    LL_GCLK_Channel_SERCOM5_CORE      = 24,
    LL_GCLK_Channel_TCC0              = 25,
    LL_GCLK_Channel_TCC1              = LL_GCLK_Channel_TCC0,
    LL_GCLK_Channel_TCC2              = 26,
    LL_GCLK_Channel_TC0               = 27,
    LL_GCLK_Channel_TC1               = LL_GCLK_Channel_TC0,
    LL_GCLK_Channel_TC2               = 28,
    LL_GCLK_Channel_TC3               = LL_GCLK_Channel_TC2,
    LL_GCLK_Channel_TC4               = 29,
    LL_GCLK_Channel_ADC               = 30,
    LL_GCLK_Channel_AC                = 31,
    LL_GCLK_Channel_DAC               = 32,
    LL_GCLK_Channel_PTC               = 33,
    LL_GCLK_Channel_CCL               = 34
};


void LL_GCLK_Reset(void);
void LL_GCLK_EnableGenerator(uint32_t generatorIndex, uint32_t enable, enum LL_GCLK_ClockSource clockSource, uint32_t divisionFactor, uint32_t runInStandby, uint32_t divideSelection, uint32_t outputEnable, uint32_t outputOffValue);
void LL_GCLK_EnablePeripheralChannel(enum LL_GCLK_Channel channel, uint32_t generatorIndex, uint32_t enable);


#ifdef __cplusplus
}
#endif

#endif // LL_GCLK_INCLUDED
