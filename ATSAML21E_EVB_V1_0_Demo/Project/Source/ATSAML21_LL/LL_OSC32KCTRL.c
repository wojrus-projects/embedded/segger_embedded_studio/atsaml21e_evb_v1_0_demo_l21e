#include <sam.h>
#include "LL_OSC32KCTRL.h"


void LL_OSC32KCTRL_XOSC32K_Initialize(uint32_t crystalOscillatorEnable,
                                      uint32_t output32kHzEnable,
                                      uint32_t output1kHzEnable,
                                      enum LL_OSC32KCTRL_XOSC32K_OscillatorStartUpTime oscillatorStartUpTime,
                                      uint32_t writeLock)
{
    OSC32KCTRL_XOSC32K_Type xosc32k = OSC32KCTRL->XOSC32K;

    xosc32k.bit.XTALEN = (crystalOscillatorEnable != 0);
    xosc32k.bit.EN32K = (output32kHzEnable != 0);
    xosc32k.bit.EN1K = (output1kHzEnable != 0);
    xosc32k.bit.STARTUP = oscillatorStartUpTime & 0x7;
    xosc32k.bit.WRTLOCK = (writeLock != 0);

    OSC32KCTRL->XOSC32K = xosc32k;
}


void LL_OSC32KCTRL_XOSC32K_Enable(uint32_t enable, uint32_t runInStandby, uint32_t onDemand)
{
    OSC32KCTRL_XOSC32K_Type xosc32k = OSC32KCTRL->XOSC32K;

    xosc32k.bit.ENABLE = (enable != 0);
    xosc32k.bit.RUNSTDBY = (runInStandby != 0);
    xosc32k.bit.ONDEMAND = (onDemand != 0);

    OSC32KCTRL->XOSC32K = xosc32k;

    if (enable != 0)
    {
        while (OSC32KCTRL->STATUS.bit.XOSC32KRDY == 0)
        {
        }
    }
}


void LL_OSC32KCTRL_OSC32K_Initialize(uint32_t output32kHzEnable,
                                     uint32_t output1kHzEnable,
                                     enum LL_OSC32KCTRL_OSC32K_OscillatorStartUpTime oscillatorStartUpTime,
                                     uint32_t oscillatorCalibration,
                                     uint32_t writeLock)
{
    OSC32KCTRL_OSC32K_Type osc32k = OSC32KCTRL->OSC32K;

    osc32k.bit.EN32K = (output32kHzEnable != 0);
    osc32k.bit.EN1K = (output1kHzEnable != 0);
    osc32k.bit.STARTUP = oscillatorStartUpTime & 0x7;
    osc32k.bit.CALIB = oscillatorCalibration & 0x7F;
    osc32k.bit.WRTLOCK = (writeLock != 0);

    OSC32KCTRL->OSC32K = osc32k;
}


void LL_OSC32KCTRL_OSC32K_Enable(uint32_t enable, uint32_t runInStandby, uint32_t onDemand)
{
    OSC32KCTRL_OSC32K_Type osc32k = OSC32KCTRL->OSC32K;

    osc32k.bit.ENABLE = (enable != 0);
    osc32k.bit.RUNSTDBY = (runInStandby != 0);
    osc32k.bit.ONDEMAND = (onDemand != 0);

    OSC32KCTRL->OSC32K = osc32k;

    if (enable != 0)
    {
        while (OSC32KCTRL->STATUS.bit.OSC32KRDY == 0)
        {
        }
    }
}


void LL_OSC32KCTRL_OSCULP32K_Initialize(uint32_t oscillatorCalibration, uint32_t writeLock)
{
    OSC32KCTRL_OSCULP32K_Type osculp32k;

    osculp32k.reg = 0;
    osculp32k.bit.CALIB = oscillatorCalibration & 0x1F;
    osculp32k.bit.WRTLOCK = (writeLock != 0);

    OSC32KCTRL->OSCULP32K = osculp32k;
}


void LL_OSC32KCTRL_Select_RTC_Clock(enum LL_OSC32KCTRL_RTC_Clock clockSource)
{
    OSC32KCTRL->RTCCTRL.bit.RTCSEL = clockSource & 0x7;
}


enum LL_OSC32KCTRL_Status LL_OSC32KCTRL_GetStatus(void)
{
    return OSC32KCTRL->STATUS.reg;
}


void LL_OSC32KCTRL_InterruptEnableClear(enum LL_OSC32KCTRL_InterruptFlag interruptFlag)
{
    OSC32KCTRL->INTENCLR.reg = interruptFlag;
}


void LL_OSC32KCTRL_InterruptEnableSet(enum LL_OSC32KCTRL_InterruptFlag interruptFlag)
{
    OSC32KCTRL->INTENSET.reg = interruptFlag;
}


enum LL_OSC32KCTRL_InterruptFlag LL_OSC32KCTRL_GetInterruptFlag(void)
{
    return OSC32KCTRL->INTFLAG.reg;
}


void LL_OSC32KCTRL_ClearInterruptFlag(enum LL_OSC32KCTRL_InterruptFlag interruptFlag)
{
    OSC32KCTRL->INTFLAG.reg = interruptFlag;
}
