#include <sam.h>
#include "LL_SERCOM_SPI.h"


static Sercom* const SERCOM_Instances[SERCOM_INST_NUM] = SERCOM_INSTS;


void LL_SERCOM_SPI_Reset(uint32_t sercomIndex)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->SPI.CTRLA.bit.SWRST = 1;

    while (pSercom->SPI.SYNCBUSY.bit.SWRST == 1)
    {
    }
}


void LL_SERCOM_SPI_Enable(uint32_t sercomIndex, uint32_t enable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->SPI.CTRLA.bit.ENABLE = (enable != 0);

    while (pSercom->SPI.SYNCBUSY.bit.ENABLE == 1)
    {
    }
}


void LL_SERCOM_SPI_SetRunInStandby(uint32_t sercomIndex, uint32_t runInStandby)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->SPI.CTRLA.bit.RUNSTDBY = (runInStandby != 0);
}


void LL_SERCOM_SPI_SetDebugControl(uint32_t sercomIndex, uint32_t runInDebugHaltedState)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->SPI.DBGCTRL.reg = (runInDebugHaltedState != 0) << SERCOM_SPI_DBGCTRL_DBGSTOP_Pos;
}


void LL_SERCOM_SPI_SetPins(uint32_t sercomIndex, enum LL_SERCOM_SPI_DataOutPinput dataOutPinout, enum LL_SERCOM_SPI_DataInPinput dataInPinout)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    SERCOM_SPI_CTRLA_Type ctrla = pSercom->SPI.CTRLA;

    ctrla.bit.DOPO = dataOutPinout & 0x3;
    ctrla.bit.DIPO = dataInPinout & 0x3;

    pSercom->SPI.CTRLA = ctrla;
}


void LL_SERCOM_SPI_SetMode(uint32_t sercomIndex, enum LL_SERCOM_SPI_Mode mode)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->SPI.CTRLA.bit.MODE = mode & 0x7;
}


void LL_SERCOM_SPI_SetDataFormat(uint32_t sercomIndex,
                                 enum LL_SERCOM_SPI_FrameFormat frameFormat,
                                 enum LL_SERCOM_SPI_ClockPhase clockPhase,
                                 enum LL_SERCOM_SPI_ClockPolarity clockPolarity,
                                 enum LL_SERCOM_SPI_DataOrder dataOrder)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    SERCOM_SPI_CTRLA_Type ctrla = pSercom->SPI.CTRLA;

    ctrla.bit.FORM = frameFormat & 0xF;
    ctrla.bit.CPHA = clockPhase & 0x1;
    ctrla.bit.CPOL = clockPolarity & 0x1;
    ctrla.bit.DORD = dataOrder & 0x1;

    pSercom->SPI.CTRLA = ctrla;
}


void LL_SERCOM_SPI_SetBaudRate(uint32_t sercomIndex, uint8_t baudRate)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->SPI.BAUD.reg = baudRate;
}


void LL_SERCOM_SPI_SetAddressMode(uint32_t sercomIndex, enum LL_SERCOM_SPI_AddressMode addressMode)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->SPI.CTRLB.bit.AMODE = addressMode & 0x3;
}


void LL_SERCOM_SPI_SetAddress(uint32_t sercomIndex, uint8_t address, uint8_t addressMask)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->SPI.ADDR.bit.ADDR = address;
    pSercom->SPI.ADDR.bit.ADDRMASK = addressMask;
}


void LL_SERCOM_SPI_SetCharacterSize(uint32_t sercomIndex, enum LL_SERCOM_SPI_CharacterSize size)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->SPI.CTRLB.bit.CHSIZE = size & 0x7;
}


void LL_SERCOM_SPI_EnableSlaveDataPreload(uint32_t sercomIndex, uint32_t enable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->SPI.CTRLB.bit.PLOADEN = (enable != 0);
}


void LL_SERCOM_SPI_SetSlaveSelect(uint32_t sercomIndex, uint32_t slaveSelectLowDetectEnable, uint32_t masterSlaveSelectEnable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    SERCOM_SPI_CTRLB_Type ctrlb = pSercom->SPI.CTRLB;

    ctrlb.bit.SSDE = (slaveSelectLowDetectEnable != 0);
    ctrlb.bit.MSSEN = (masterSlaveSelectEnable != 0);

    pSercom->SPI.CTRLB = ctrlb;
}


void LL_SERCOM_SPI_SetImmediateBufferOverflowNotification(uint32_t sercomIndex, uint32_t state)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->SPI.CTRLA.bit.IBON = (state != 0);
}


void LL_SERCOM_SPI_EnableReceiver(uint32_t sercomIndex, uint32_t enable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->SPI.CTRLB.bit.RXEN = (enable != 0);

    while (pSercom->SPI.SYNCBUSY.bit.CTRLB == 1)
    {
    }
}


enum LL_SERCOM_SPI_Status LL_SERCOM_SPI_GetStatus(uint32_t sercomIndex)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    return pSercom->SPI.STATUS.reg;
}


uint16_t LL_SERCOM_SPI_ReadData(uint32_t sercomIndex)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    return pSercom->SPI.DATA.reg;
}


void LL_SERCOM_SPI_WriteData(uint32_t sercomIndex, uint16_t dataTx)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->SPI.DATA.reg = dataTx;
}


void LL_SERCOM_SPI_InterruptEnableClear(uint32_t sercomIndex, enum LL_SERCOM_SPI_InterruptFlag interruptFlag)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->SPI.INTENCLR.reg = interruptFlag;
}


void LL_SERCOM_SPI_InterruptEnableSet(uint32_t sercomIndex, enum LL_SERCOM_SPI_InterruptFlag interruptFlag)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->SPI.INTENSET.reg = interruptFlag;
}


enum LL_SERCOM_SPI_InterruptFlag LL_SERCOM_SPI_GetInterruptEnableState(uint32_t sercomIndex)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    return pSercom->SPI.INTENSET.reg;
}


enum LL_SERCOM_SPI_InterruptFlag LL_SERCOM_SPI_GetInterruptFlag(uint32_t sercomIndex)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    return  pSercom->SPI.INTFLAG.reg;
}


void LL_SERCOM_SPI_ClearInterruptFlag(uint32_t sercomIndex, enum LL_SERCOM_SPI_InterruptFlag interruptFlag)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->SPI.INTFLAG.reg = interruptFlag;
}
