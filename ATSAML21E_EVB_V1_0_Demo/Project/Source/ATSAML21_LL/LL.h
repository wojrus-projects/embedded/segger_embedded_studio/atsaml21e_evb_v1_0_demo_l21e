//
// Peripheral Driver Low Layer all modules.
//

#ifndef LL_INCLUDED
#define LL_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif


#include <sam.h>
#include "LL_DSU.h"
#include "LL_GCLK.h"
#include "LL_MCLK.h"
#include "LL_RSTC.h"
#include "LL_PM.h"
#include "LL_OSCCTRL.h"
#include "LL_OSC32KCTRL.h"
#include "LL_SUPC.h"
#include "LL_WDT.h"
#include "LL_RTC.h"
#include "LL_DMAC.h"
#include "LL_EIC.h"
#include "LL_NVMCTRL.h"
#include "LL_PORT.h"
#include "LL_EVSYS.h"
#include "LL_SERCOM_USART.h"
#include "LL_SERCOM_SPI.h"
#include "LL_SERCOM_I2C_MASTER.h"
#include "LL_SERCOM_I2C_SLAVE.h"
#include "LL_TC.h"
#include "LL_TCC.h"
#include "LL_TRNG.h"
#include "LL_AES.h"
#include "LL_CCL.h"
#include "LL_OPAMP.h"
#include "LL_ADC.h"
#include "LL_AC.h"
#include "LL_DAC.h"
#include "LL_PAC.h"


#ifdef __cplusplus
}
#endif

#endif // LL_INCLUDED
