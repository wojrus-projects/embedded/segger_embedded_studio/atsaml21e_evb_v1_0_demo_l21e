//
// Peripheral Driver Low Layer (RSTC module).
//

#ifndef LL_RSTC_INCLUDED
#define LL_RSTC_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif


enum LL_RSTC_ResetCause
{
    LL_RSTC_ResetCause_POR,
    LL_RSTC_ResetCause_BOD12,
    LL_RSTC_ResetCause_BOD33,
    LL_RSTC_ResetCause_EXT,
    LL_RSTC_ResetCause_WDT,
    LL_RSTC_ResetCause_SYST,
    LL_RSTC_ResetCause_BACKUP
};


enum LL_RSTC_ResetCause LL_RSTC_GetResetCause(void);


#ifdef __cplusplus
}
#endif

#endif // LL_RSTC_INCLUDED
