//
// EVB resources.
//

#include <stdio.h>

#include "ATSAML21_LL/LL.h"
#include "ATSAML21_LL_Test/LL_Test_Main.h"
#include "Board.h"
#include "Keypad.h"
#include "DAC.h"
#include "tusb.h"


// NVM User Row custom parameters at 0x00804000.
const struct LL_NVMCTRL_UserRow __attribute__ ((section(".user_row"), used)) User_Row_Application =
{
    LL_NVMCTRL_USER_ROW_FACTORY_VALUE
};

// Obszar RAM zajmowany przez Micro Trace Buffer.
// Musi mieć rozmiar 16-32768 bajtów (wielokrotność 2).
//
// Opcje projektu do ustawienia:
// - Debug->Target Trace->Trace Interface Type = MTB
// - Debug->Target Trace->MTB RAM Address = 0x20000000
// - Debug->Target Trace->MTB RAM Size = 128
//
// Dane z MTB można oglądać w View->More Debug Windows->Exection Trace.
uint8_t MTB_Buffer[128] __attribute__((section(".mtb_buffer")));


struct PinDescriptor
{
    uint8_t portNumber;
    uint8_t pinNumber;
};


static const struct PinDescriptor Pin_Button_Array[] =
{
    { PORT_BUTTON_1, PIN_BUTTON_1 },
    { PORT_BUTTON_2, PIN_BUTTON_2 }
};


static const struct PinDescriptor Pin_LED_Array[] =
{
    { PORT_LED_1, PIN_LED_1 },
    { PORT_LED_2, PIN_LED_2 }
};


static void Watchdog_Initialize(void);
static void USB_Initialize(void);


void Board_Initialize(void)
{
#if LL_TEST == 0
    Watchdog_Initialize();
#endif

    //
    // Print reset cause
    //

    const char * const resetCauseNames[] =
    {
        "POR",
        "BOD12",
        "BOD33",
        "EXT",
        "WDT",
        "SYST",
        "BACKUP"
    };

    const enum LL_RSTC_ResetCause resetCause = LL_RSTC_GetResetCause();

    const char * pResetCauseName;

    if (resetCause > LL_RSTC_ResetCause_BACKUP)
    {
        pResetCauseName = "UNKNOWN";
    }
    else
    {
        pResetCauseName = resetCauseNames[resetCause];
    }

    printf("\nReset cause: %s\n", pResetCauseName);

    //
    // Print MCU identification data
    //

    const struct LL_DSU_DeviceIdentification did = LL_DSU_GetDeviceIdentification();

    printf("MCU identification (DSU.DID):\nprocessor: %u, family: %u, series: %u, die: %u, revision: %u, selection: %u\n",
        did.processor, did.productFamily, did.productSeries, did.dieNumber, did.revisionNumber, did.deviceSelection);

    //
    // Print MCU S/N
    //

    uint8_t serialNumber[LL_NVMCTRL_SERIAL_NUMBER_LENGTH_BYTES];

    LL_NVMCTRL_ReadSerialNumber(serialNumber);

    printf("MCU S/N: ");

    for (size_t i = 0; i < sizeof(serialNumber); i++)
    {
        if ((i > 0) && (i % 4 == 0))
        {
            printf("-");
        }

        printf("%02X", serialNumber[i]);
    }

    printf("\n");

    //
    // Print NVM parameters
    //

    struct LL_NVMCTRL_Parameters NVM_Parameters = LL_NVMCTRL_GetParameter();

    printf("NVM pages: %u\n", NVM_Parameters.NVM_PagesNumber);
    printf("RWW EEPROM pages: %u\n", NVM_Parameters.RWW_EEPROM_PagesNumber);
    printf("NVM page size: %u bytes\n", NVM_Parameters.PageSizeBytes);
    printf("Flash size: %u bytes\n", NVM_Parameters.NVM_PagesNumber * NVM_Parameters.PageSizeBytes);

    //
    // Print NVM User Row
    //

    struct LL_NVMCTRL_UserRow userRow;

    LL_NVMCTRL_ReadUserRow(&userRow);

    printf("\nNVM User Row:\n");
    printf("BOOTPROT:         0x%X\n", userRow.BOOTPROT);
    printf("Reserved_1:       0x%X\n", userRow.Reserved_1);
    printf("EEPROM:           0x%X\n", userRow.EEPROM);
    printf("Reserved_2:       0x%X\n", userRow.Reserved_2);
    printf("BOD33_Level:      0x%X\n", userRow.BOD33_Level);
    printf("BOD33_Disable:    0x%X\n", userRow.BOD33_Disable);
    printf("BOD33_Action:     0x%X\n", userRow.BOD33_Action);
    printf("Reserved_3:       0x%X\n", userRow.Reserved_3);
    printf("WDT_Enable:       0x%X\n", userRow.WDT_Enable);
    printf("WDT_AlwaysOn:     0x%X\n", userRow.WDT_AlwaysOn);
    printf("WDT_Period:       0x%X\n", userRow.WDT_Period);
    printf("WDT_Window:       0x%X\n", userRow.WDT_Window);
    printf("WDT_EWOFFSET:     0x%X\n", userRow.WDT_EWOFFSET);
    printf("WDT_WEN:          0x%X\n", userRow.WDT_WEN);
    printf("BOD33_Hysteresis: 0x%X\n", userRow.BOD33_Hysteresis);
    printf("BOD12_Hysteresis: 0x%X\n", userRow.BOD12_Hysteresis);
    printf("Reserved_4:       0x%X\n", userRow.Reserved_4);
    printf("LOCK:             0x%X\n", userRow.LOCK);

    //
    // Print DSU status
    //

    const enum LL_DSU_StatusB dsuStatus = LL_DSU_GetStatusB();

    printf("\nDSU status:\n");
    printf("Device protection: %u\n", (dsuStatus & LL_DSU_StatusB_PROT) != 0);
    printf("Debugger present: %u\n", (dsuStatus & LL_DSU_StatusB_DBGPRES) != 0);
    printf("Hot-plugging enable: %u\n\n", (dsuStatus & LL_DSU_StatusB_HPE) != 0);

    //
    // Set VREG.
    //

    LL_SUPC_VREG_SelectType(LL_SUPC_VREG_LDO);
    LL_SUPC_VREG_SetLowPowerModeEfficiency(LL_SUPC_VREG_LowPowerModeEfficiency_Default);
    LL_SUPC_InitializeStandbySleepMode(LL_SUPC_VREG_PowerModeInStandby_Normal, LL_SUPC_VREG_PerformanceLevelInStandby_Current);

    //
    // Set cache and clocks
    //

    LL_NVMCTRL_SetCache(0, LL_NVMCTRL_CacheReadMode_NO_MISS_PENALTY);
    LL_NVMCTRL_SetReadWaitStates(2);
    LL_NVMCTRL_SetPowerReductionMode(LL_NVMCTRL_PowerReductionMode_WAKEUPINSTANT);

    LL_PM_SetPerformanceLevel(0, LL_PM_PerformanceLevel_PL2);

    const uint32_t MCLK_Bridge = 0;
    const uint32_t MCLK_Index = 1;

    LL_PAC_SetWriteControl(LL_PAC_ControlMode_CLEAR, MCLK_Bridge, MCLK_Index);

    LL_MCLK_InitializeClocks();
    LL_MCLK_SetClockDivision(LL_MCLK_CPU_Clock_Division_DIV1, LL_MCLK_Low_Power_Clock_Division_DIV1, LL_MCLK_Backup_Clock_Division_DIV1);

    LL_GCLK_Reset();

    // Enable XTAL = 16,0 MHz
    LL_OSCCTRL_XOSC_Enable(1, 1, 1, 0, LL_OSCCTRL_XOSC_Gain_For_16MHz, 1, LL_OSCCTRL_XOSC_StartUpTime_8ms);
    LL_GCLK_EnableGenerator(0, 1, LL_GCLK_ClockSource_XOSC, 0, 1, 0, 0, 0);

    // Enable DPLL with XTAL
    //
    // f_xosc = 16,0 MHz
    // f_sys = f_xosc x DPLL_Ratio = 16,0 x 3 = 48,0 MHz
    // f_in_max = f_xosc / (2 x (clockDivider + 1)) = 2,0 MHz -> clockDivider = 3
    // loopDividerRatio = f_sys / f_in_max - 1 = 48 MHz / 2 MHz - 1 = 23
    // loopDividerRatioFractionalPart = 0

    const uint32_t clockDivider = 3;
    const uint32_t loopDividerRatio = 23;
    const uint32_t loopDividerRatioFractionalPart = 0;

    LL_OSCCTRL_DPLL_Initialize(LL_OSCCTRL_DPLL_Filter_HBFILT, 0, 0, LL_OSCCTRL_DPLL_LockTime_DEFAULT, 0);
    LL_OSCCTRL_DPLL_SetClock(LL_OSCCTRL_DPLL_ReferenceClock_XOSC, clockDivider);
    LL_OSCCTRL_DPLL_SetPrescaler(LL_OSCCTRL_DPLL_Prescaler_DIV1);
    LL_OSCCTRL_DPLL_SetRatio(loopDividerRatio, loopDividerRatioFractionalPart);
    LL_OSCCTRL_DPLL_Enable(1, 1, 0);

#if LL_TEST == 0
    #warning "Enable DPLL clock output GCLK_IO[6] on pin PA22 (CLICK_SDA)"
    LL_GCLK_EnableGenerator(6, 1, LL_GCLK_ClockSource_DPLL96M, 0, 1, 0, 1, 0);
    LL_PORT_EnablePeripheralMultiplexer(PORT_CLK_SYS, PIN_CLK_SYS, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLK_SYS, PIN_CLK_SYS, LL_PORT_PeripheralFunction_H);
#endif

    // Switch system clock to DPLL
    LL_GCLK_EnableGenerator(0, 1, LL_GCLK_ClockSource_DPLL96M, 0, 1, 0, 0, 0);

    // Disable OSC16M
    LL_OSCCTRL_OSC16M_Enable(0, LL_OSCCTRL_OSC16M_Frequency_16MHz, 1, 0);

    //
    // GPIO outputs
    //

    LL_PORT_EnablePeripheralMultiplexer(PORT_LED_1, PIN_LED_1, 0);
    LL_PORT_EnablePeripheralMultiplexer(PORT_LED_2, PIN_LED_2, 0);
    LL_PORT_SetDirection(PORT_LED_1, PIN_LED_1, LL_PORT_PinDirection_Output);
    LL_PORT_SetDirection(PORT_LED_2, PIN_LED_2, LL_PORT_PinDirection_Output);
    LL_PORT_SetOutputDriverStrength(PORT_LED_1, PIN_LED_1, LL_PORT_OutputDriverStrength_High);
    LL_PORT_SetOutputDriverStrength(PORT_LED_2, PIN_LED_2, LL_PORT_OutputDriverStrength_High);

    //
    // GPIO inputs
    //

    LL_PORT_EnablePeripheralMultiplexer(PORT_BUTTON_1, PIN_BUTTON_1, 0);
    LL_PORT_EnablePeripheralMultiplexer(PORT_BUTTON_2, PIN_BUTTON_2, 0);
    LL_PORT_SetDirection(PORT_BUTTON_1, PIN_BUTTON_1, LL_PORT_PinDirection_Input);
    LL_PORT_SetDirection(PORT_BUTTON_2, PIN_BUTTON_2, LL_PORT_PinDirection_Input);
    LL_PORT_SetPullMode(PORT_BUTTON_1, PIN_BUTTON_1, LL_PORT_PullMode_Up);
    LL_PORT_SetPullMode(PORT_BUTTON_2, PIN_BUTTON_2, LL_PORT_PullMode_Up);
    LL_PORT_EnableInput(PORT_BUTTON_1, PIN_BUTTON_1, 1);
    LL_PORT_EnableInput(PORT_BUTTON_2, PIN_BUTTON_2, 1);
    LL_PORT_EnableInputSynchronizer(PORT_BUTTON_1, PIN_BUTTON_1, 1);
    LL_PORT_EnableInputSynchronizer(PORT_BUTTON_2, PIN_BUTTON_2, 1);

    //
    // Buttons debouncer
    //

    Keypad_Initialize();

    //
    // SysTick
    //

    NVIC_SetPriority(SysTick_IRQn, 0);
    SysTick_Config(SYSTEM_CLOCK_FREQUENCY_Hz / 1000);

    //
    // Optional LL library tests
    //

#if LL_TEST == 1
    LL_Test_Main();
#endif

    //
    // DAC
    //

    DAC_Initialize(true, true);
    DAC_SetOutput(DAC_0, 0.0f);
    DAC_SetOutput(DAC_1, 0.0f);

    //
    // USB Device
    //

    USB_Initialize();
}


void LED_Set(enum LED ledIndex, bool state)
{
    if (ledIndex >= LED_NUM)
    {
        return;
    }

    const struct PinDescriptor pin = Pin_LED_Array[ledIndex];

    LL_PORT_SetOutput(pin.portNumber, pin.pinNumber, state);
}


void LED_Toggle(enum LED ledIndex)
{
    if (ledIndex >= LED_NUM)
    {
        return;
    }

    const struct PinDescriptor pin = Pin_LED_Array[ledIndex];

    LL_PORT_ToggleOutput(pin.portNumber, pin.pinNumber);
}


bool Button_Get(enum Button buttonIndex)
{
    if (buttonIndex >= BUTTON_NUM)
    {
        return false;
    }

    const struct PinDescriptor pin = Pin_Button_Array[buttonIndex];

    return (LL_PORT_ReadInput(pin.portNumber, pin.pinNumber) != 0);
}


static void Watchdog_Initialize(void)
{
    if (LL_WDT_IsEnabled() == 0)
    {
        LL_WDT_SetConfiguration(LL_WDT_TimeOutPeriod_CYC4096, LL_WDT_WindowTimeOutPeriod_CYC2048);
        LL_WDT_SetEarlyWarningInterruptTimeOffset(LL_WDT_EarlyWarningTimeOffset_CYC2048);

        LL_WDT_ClearInterruptFlag();
        LL_WDT_InterruptEnableSet();

        NVIC_ClearPendingIRQ(WDT_IRQn);
        NVIC_SetPriority(WDT_IRQn, 0);
        NVIC_EnableIRQ(WDT_IRQn);

        LL_WDT_Clear();
        LL_WDT_Enable(1, 0, 0);
    }
}


static void USB_Initialize(void)
{
    MCLK->AHBMASK.bit.USB_ = 1;
    MCLK->APBBMASK.bit.USB_ = 1;

    const struct LL_NVMCTRL_SoftwareCalibrationData calibrationData = LL_NVMCTRL_ReadSoftwareCalibrationData();

    // Start clock 48 MHz for USB module
    LL_OSCCTRL_DFLL48M_Enable(1, 1, 0, LL_OSCCTRL_DFLL48M_Mode_OpenLoop);
    LL_OSCCTRL_DFLL48M_SetValue(calibrationData.DFLL48M_COARSE_CAL, LL_OSCCTRL_DFLL48M_FINE_STEPS / 2);

    // Connect clock source to USB module.
    LL_GCLK_EnableGenerator(7, 1, LL_GCLK_ClockSource_DFLL48M, 0, 1, 0, 1, 0);
    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_USB, 7, 1);

    #warning "Enable USB clock output GCLK_IO[7] on pin PA23 (CLICK_SCL)"
    LL_PORT_EnablePeripheralMultiplexer(PORT_CLK_USB, PIN_CLK_USB, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLK_USB, PIN_CLK_USB, LL_PORT_PeripheralFunction_H);

    // Set USB pins
    LL_PORT_EnablePeripheralMultiplexer(PORT_USB_DM, PIN_USB_DM, 1);
    LL_PORT_EnablePeripheralMultiplexer(PORT_USB_DP, PIN_USB_DP, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_USB_DM, PIN_USB_DM, LL_PORT_PeripheralFunction_G);
    LL_PORT_SetPeripheralMultiplexing(PORT_USB_DP, PIN_USB_DP, LL_PORT_PeripheralFunction_G);

    tusb_init();
}
